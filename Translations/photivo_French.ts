<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ChannelMixerForm</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="31"/>
        <source>Load</source>
        <translation type="unfinished">Charger</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="42"/>
        <source>Save</source>
        <translation type="unfinished">Enregistrer</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="82"/>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="98"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="108"/>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="167"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="151"/>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.ui" line="177"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
</context>
<context>
    <name>ExposureForm</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.ui" line="93"/>
        <source>Automatic exposure calculation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.ui" line="27"/>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.ui" line="34"/>
        <source>Use maximum radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.ui" line="56"/>
        <source>Adaptive saturation</source>
        <translation type="unfinished">Saturation adaptative</translation>
    </message>
</context>
<context>
    <name>Global Strings</name>
    <message>
        <location filename="../Sources/ptMain.cpp" line="175"/>
        <source>Photivo channelmixer file (*.ptm);;All files (*.*)</source>
        <translation>Fichier du mixeur de canaux Photivo (*.ptm);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="177"/>
        <source>Photivo curve file (*.ptc);;All files (*.*)</source>
        <translation>Fichier de courbe photivo (*.ptc);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="179"/>
        <source>Photivo job file (*.ptj);;All files (*.*)</source>
        <translation>Fichier de tâches Photivo (*.ptj);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="181"/>
        <source>Photivo settings file (*.pts);;All files (*.*)</source>
        <translation>Fichier de configuration de Photivo (*.pts);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="183"/>
        <source>ICC colour profiles (*.icc *.icm);;All files (*.*)</source>
        <translation>Profils de couleurs ICC (*.icc *.icm);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="187"/>
        <source>Raw files (*.arw *.ARW *.Arw *.bay *.BAY *.Bay *.bmq *.BMQ *.Bmq *.cr2 *.CR2 *.Cr2 *.crw *.CRW *.Crw *.cs1 *.CS1 *.Cs1 *.dc2 *.DC2 *.Dc2 *.dcr *.DCR *.Dcr *.dng *.DNG *.Dng *.erf *.ERF *.Erf *.fff *.FFF *.Fff *.hdr *.HDR *.Hdr *.ia  *.IA *.Ia *.k25 *.K25 *.kc2 *.KC2 *.Kc2 *.kdc *.KDC *.Kdc *.mdc *.MDC *.Mdc *.mef *.MEF *.Mef *.mos *.MOS *.Mos *.mrw *.MRW *.Mrw *.nef *.NEF *.Nef *.nrw *.NRW *.Nrw *.orf *.ORF *.Orf *.pef *.PEF *.Pef *.pxn *.PXN *.Pxn *.qtk *.QTK *.Qtk *.raf *.RAF *.Raf *.raw *.RAW *.Raw *.rdc *.RDC *.Rdc *.rw2 *.RW2 *.Rw2 *.sr2 *.SR2 *.Sr2 *.srf *.SRF *.Srf *.srw *.SRW *.Srw *.sti *.STI *.Sti *.tif *.TIF *.Tif *.x3f *.X3F *.X3f);;Bitmaps (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.png *.PNG *.Png *.ppm *.PPm *.Ppm);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="235"/>
        <source>Bitmaps (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.png *.PNG *.Png *.ppm *.PPm *.Ppm ;;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="246"/>
        <source>Jpeg (*.jpg);;Tiff (*.tiff);;Png (*.png);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Raw files (*.arw *.ARW *.Arw *.bay *.BAY *.Bay *.bmq *.BMQ *.Bmq *.cr2 *.CR2 *.Cr2 *.crw *.CRW *.Crw *.cs1 *.CS1 *.Cs1 *.dc2 *.DC2 *.Dc2 *.dcr *.DCR *.Dcr *.dng *.DNG *.Dng *.erf *.ERF *.Erf *.fff *.FFF *.Fff *.hdr *.HDR *.Hdr *.ia  *.IA *.Ia *.k25 *.K25 *.kc2 *.KC2 *.Kc2 *.kdc *.KDC *.Kdc *.mdc *.MDC *.Mdc *.mef *.MEF *.Mef *.mos *.MOS *.Mos *.mrw *.MRW *.Mrw *.nef *.NEF *.Nef *.orf *.ORF *.Orf *.pef *.PEF *.Pef *.pxn *.PXN *.Pxn *.qtk *.QTK *.Qtk *.raf *.RAF *.Raf *.raw *.RAW *.Raw *.rdc *.RDC *.Rdc *.rw2 *.RW2 *.Rw2 *.sr2 *.SR2 *.Sr2 *.srf *.SRF *.Srf *.sti *.STI *.Sti *.tif *.TIF *.Tif *.x3f *.X3F *.X3f);;Bitmaps (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.ppm *.PPm *.Ppm ;;All files (*.*)</source>
        <translation type="obsolete">Fichiers raw (*.arw *.ARW *.Arw *.bay *.BAY *.Bay *.bmq *.BMQ *.Bmq *.cr2 *.CR2 *.Cr2 *.crw *.CRW *.Crw *.cs1 *.CS1 *.Cs1 *.dc2 *.DC2 *.Dc2 *.dcr *.DCR *.Dcr *.dng *.DNG *.Dng *.erf *.ERF *.Erf *.fff *.FFF *.Fff *.hdr *.HDR *.Hdr *.ia  *.IA *.Ia *.k25 *.K25 *.kc2 *.KC2 *.Kc2 *.kdc *.KDC *.Kdc *.mdc *.MDC *.Mdc *.mef *.MEF *.Mef *.mos *.MOS *.Mos *.mrw *.MRW *.Mrw *.nef *.NEF *.Nef *.orf *.ORF *.Orf *.pef *.PEF *.Pef *.pxn *.PXN *.Pxn *.qtk *.QTK *.Qtk *.raf *.RAF *.Raf *.raw *.RAW *.Raw *.rdc *.RDC *.Rdc *.rw2 *.RW2 *.Rw2 *.sr2 *.SR2 *.Sr2 *.srf *.SRF *.Srf *.sti *.STI *.Sti *.tif *.TIF *.Tif *.x3f *.X3F *.X3f);;Fichiers bitmap (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.ppm *.PPm *.Ppm) ;;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Bitmaps (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.ppm *.PPm *.Ppm ;;All files (*.*)</source>
        <translation type="obsolete">Fichiers bitmap (*.jpeg *.JPEG *.Jpeg *.jpg *.JPG *.Jpg *.tiff *.TIFF *.Tiff *.tif *.TIF *.Tif *.bmp *.BMP *.Bmp *.ppm *.PPm *.Ppm) ;;Tous les fichiers (*.*)</translation>
    </message>
</context>
<context>
    <name>GradientSharpenForm</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.ui" line="57"/>
        <source>Microcontrast</source>
        <translation type="unfinished">Micro-contraste</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sources/ptCimg.cpp" line="95"/>
        <source>GreycStoration iteration </source>
        <translation>Itération GreycStoration</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="118"/>
        <source>Flood fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="119"/>
        <source>Search</source>
        <translation type="unfinished">Rechercher</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="123"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="124"/>
        <source>Zoom fit</source>
        <translation>Zoom adapté</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="125"/>
        <source>5%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="126"/>
        <source>10%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="127"/>
        <source>25%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="128"/>
        <source>33%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="129"/>
        <source>50%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="130"/>
        <source>66%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="131"/>
        <source>100%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="132"/>
        <source>150%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="133"/>
        <source>200%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="134"/>
        <source>300%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="135"/>
        <source>400%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="139"/>
        <source>Standard file in the Photivo directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="140"/>
        <source>The file batch list was previously saved to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="144"/>
        <source>None: even this setting is lost ...</source>
        <translation>Aucun : même ce paramètre est perdu...</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="145"/>
        <source>Minimal: dirs, available curves ...</source>
        <translation>Minimal : dossiers, courbes disponibles...</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="146"/>
        <source>Medium: most used settings ...</source>
        <translation>Moyen: la plupart des paramètres utilisés...</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="147"/>
        <source>All: remember everything</source>
        <translation>Tout: se souvenir de tout</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="151"/>
        <source>Flat Profile</source>
        <translation>Profil plat</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="152"/>
        <source>Adobe Matrix</source>
        <translation>Matrice Adobe</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="153"/>
        <source>Adobe Profile</source>
        <translation>Profil Adobe</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="155"/>
        <source>External Profile</source>
        <translation>Profil externe</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="159"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="186"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="193"/>
        <source>Perceptual</source>
        <translation>Perceptuel</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="160"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="187"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="194"/>
        <source>Relative Colorimetric</source>
        <translation>Colorimétrique relatif</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="161"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="188"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="195"/>
        <source>Saturation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="162"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="189"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="196"/>
        <source>Absolute Colorimetric</source>
        <translation>Colorimétrique absolu</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="166"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="200"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="237"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="243"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="256"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="274"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="371"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="464"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="469"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="167"/>
        <source>sRGB</source>
        <translation>sRVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="168"/>
        <source>BT709</source>
        <translation>BT709</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="169"/>
        <source>Pure 2.2</source>
        <translation>2.2 pur</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="173"/>
        <source>sRGB - D65</source>
        <translation>sRVB - D65</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="174"/>
        <source>Adobe RGB - D65</source>
        <translation>Adobe RVB - D65</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="175"/>
        <source>Wide Gamut RGB - D50</source>
        <translation>Gamut RVB large - D50</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="176"/>
        <source>Kodak Pro PhotoRGB - D50</source>
        <translation>Kodak Pro PhotoRVB - D50</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="180"/>
        <source>No optimization</source>
        <translation>Aucune optimisation</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="181"/>
        <source>High res pre calc</source>
        <translation>Pré-calcul haute résolution</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="182"/>
        <source>Fast sRGB preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="64"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="201"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="37"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="43"/>
        <source>With preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="70"/>
        <source>Diamond</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="71"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="72"/>
        <source>Rectangle 1 (rounded)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="73"/>
        <source>Rectangle 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="74"/>
        <source>Rectangle 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="75"/>
        <source>Rectangle 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="76"/>
        <source>Rectangle 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="77"/>
        <source>Rectangle 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="78"/>
        <source>Rectangle 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="79"/>
        <source>Rectangle 8 (sharp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="202"/>
        <source>50% grey</source>
        <translation>Gris 50%</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="203"/>
        <source>Dark grey</source>
        <translation>Gris foncé</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="204"/>
        <source>Night</source>
        <translation>Nuit</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="208"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="209"/>
        <source>Purple</source>
        <translation>Violet</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="210"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="211"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="212"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="216"/>
        <source>Tab mode</source>
        <translation>Onglets</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="217"/>
        <source>Favourite tools</source>
        <translation>Outils favoris</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="218"/>
        <source>All tools</source>
        <translation>Tous les outils</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="222"/>
        <source>1:32</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="223"/>
        <source>1:16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="224"/>
        <source>1:8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="225"/>
        <source>1:4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="226"/>
        <source>1:2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="227"/>
        <source>1:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="231"/>
        <source>Halt</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="232"/>
        <source>Run once</source>
        <translation>Exécuter une fois</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="233"/>
        <source>Run always</source>
        <translation>Toujours exécuter</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="238"/>
        <source>Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="239"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="257"/>
        <source>3rd order polynomial</source>
        <translation>Polynôme du 3e ordre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="244"/>
        <source>6th order polynomial</source>
        <translation>Polynôme du 6e ordre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="248"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="249"/>
        <source>Rectilinear</source>
        <translation>Rectilinéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="250"/>
        <source>Fisheye</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="251"/>
        <source>Panoramic</source>
        <translation>Panoramique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="252"/>
        <source>Equirectangular</source>
        <translation>Equirectangulaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="258"/>
        <source>5th order polynomial</source>
        <translation>Polynôme du 5e ordre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="260"/>
        <source>1st order field of view</source>
        <translation>Champ de vision au 1er ordre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="262"/>
        <source>Panotools lens model</source>
        <translation>Modèle d&apos;objectif Panotools</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="266"/>
        <source>No guidelines</source>
        <translation>Pas de lignes guides</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="267"/>
        <source>Rule of thirds</source>
        <translation>Règle des tiers</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="268"/>
        <source>Golden ratio</source>
        <translation>Nombre d&apos;or</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="269"/>
        <source>Diagonals</source>
        <translation>Diagonales</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="270"/>
        <source>Center lines</source>
        <translation>Lignes centrées</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="275"/>
        <source>Dimmed</source>
        <translation>Obscurci</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="276"/>
        <source>Black</source>
        <translation>Noir</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="280"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="310"/>
        <source>Box filter</source>
        <translation>Filtre rectangulaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="281"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="311"/>
        <source>Triangle filter</source>
        <translation>Filtre bilinéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="282"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="317"/>
        <source>Quadratic filter</source>
        <translation>Filtre quadratique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="283"/>
        <source>Cubic spline filter</source>
        <translation>Filtre en spline cubique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="284"/>
        <source>Quadratic spline filter</source>
        <translation>Filtre en spline quadratique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="285"/>
        <source>Cubic convolution filter</source>
        <translation>Filtre convolutif cubique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="286"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="321"/>
        <source>Lanczos3 filter</source>
        <translation>Filtre Lanczos3</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="287"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="320"/>
        <source>Mitchell filter</source>
        <translation>Filtre de Mitchell</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="288"/>
        <source>Catmull Rom filter</source>
        <translation>Filtre de Catmull-Rom</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="289"/>
        <source>Cosine filter</source>
        <translation>Filtre en cosinus</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="290"/>
        <source>Bell filter</source>
        <translation>Filtre en cloche</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="291"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="312"/>
        <source>Hermite filter</source>
        <translation>Filtre de Hermite</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="295"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="302"/>
        <source>Longer edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="296"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="303"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="297"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="304"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="298"/>
        <source>Width x Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="309"/>
        <source>Point filter</source>
        <translation>Plus proche voisin</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="313"/>
        <source>Hanning filter</source>
        <translation>Filtre de Hanning</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="314"/>
        <source>Hamming filter</source>
        <translation>Filtre de Hamming</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="315"/>
        <source>Blackman filter</source>
        <translation>Filtre de Blackman</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="316"/>
        <source>Gaussian filter</source>
        <translation>Filtre gaussien</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="318"/>
        <source>Cubic filter</source>
        <translation>Filtre cubique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="319"/>
        <source>Catmull-Rom filter</source>
        <translation>Filtre de Catmull-Rom</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="35"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="41"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="48"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="56"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="327"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="400"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="328"/>
        <source>Directional grad brightness</source>
        <translation>Gradient de luminosité directionel</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="329"/>
        <source>Average grad brightness</source>
        <translation>Gradient de luminosité moyen</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="330"/>
        <source>Norm brightness</source>
        <translation>Normaliser la luminosité</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="331"/>
        <source>Directional grad luminance</source>
        <translation>Gradient de luminance directionel</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="332"/>
        <source>Average grad luminance</source>
        <translation>Gradient de luminance directionel</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="333"/>
        <source>Norm luminance</source>
        <translation>Normer la luminance</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="337"/>
        <source>Relative</source>
        <translation>Relatif</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="338"/>
        <source>Absolute</source>
        <translation>Absolu</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="342"/>
        <source>Camera</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="343"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="344"/>
        <source>Spot</source>
        <translation>Pipette</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="345"/>
        <source>Manual</source>
        <translation>Manuel</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="349"/>
        <source>No CA correction</source>
        <translation>Pas de correction des AC</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="350"/>
        <source>Automatic CA cor.</source>
        <translation>Correction auto des AC</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="351"/>
        <source>Manual CA cor.</source>
        <translation>Correction manuelle des AC</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="355"/>
        <source>Bilinear</source>
        <translation>Bilinéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="356"/>
        <source>VNG</source>
        <translation>VNG</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="357"/>
        <source>VNG 4 color</source>
        <translation>VNG 4 couleurs</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="358"/>
        <source>PPG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="359"/>
        <source>AHD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="360"/>
        <source>AHD modified</source>
        <translation>AHD modifié</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="361"/>
        <source>AMaZE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="362"/>
        <source>DCB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="363"/>
        <source>DCB soft</source>
        <translation>DCB doux</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="364"/>
        <source>DCB sharp</source>
        <translation>DCB doux</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="365"/>
        <source>VCD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="366"/>
        <source>LMMSE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="367"/>
        <source>Bayer pattern</source>
        <translation>Structure de Bayer</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="372"/>
        <source>FBDD 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="373"/>
        <source>FBDD 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="378"/>
        <source>Clip</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="379"/>
        <source>No clipping</source>
        <translation>Ne pas couper</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="380"/>
        <source>Restore in Lab</source>
        <translation>Restaurer en Lab</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="381"/>
        <source>Restore in HSV</source>
        <translation>Restaurer en TSV</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="382"/>
        <source>Blend in Lab</source>
        <translation>Fusionner en Lab</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="383"/>
        <source>Rebuild</source>
        <translation>Reconstruire</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="49"/>
        <source>Shadows</source>
        <translation>Ombres</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="50"/>
        <source>Midtones</source>
        <translation>Tons moyens</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="51"/>
        <source>Highlights</source>
        <translation>Hautes lumières</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="52"/>
        <source>All values</source>
        <translation>Toutes les valeurs</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="387"/>
        <source>Regular L*</source>
        <translation>L* classique</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="388"/>
        <source>R -&gt; L*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="389"/>
        <source>G -&gt; L*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="390"/>
        <source>B -&gt; L*</source>
        <translation></translation>
    </message>
    <message>
        <source>Nearest Neighbour</source>
        <translation type="vanished">Plus proche voisin</translation>
    </message>
    <message>
        <source>Shadows 1</source>
        <translation type="vanished">Ombres 1</translation>
    </message>
    <message>
        <source>Shadows 2</source>
        <translation type="vanished">Ombres 2</translation>
    </message>
    <message>
        <source>Shadows 3</source>
        <translation type="vanished">Ombres 3</translation>
    </message>
    <message>
        <source>Shadows 4</source>
        <translation type="vanished">Ombres 4</translation>
    </message>
    <message>
        <source>Shadows 5</source>
        <translation type="vanished">Ombres 5</translation>
    </message>
    <message>
        <source>Low sensitivity</source>
        <translation type="vanished">Faible sensibilité</translation>
    </message>
    <message>
        <source>High sensitivity</source>
        <translation type="vanished">Haute sensibilité</translation>
    </message>
    <message>
        <source>Hyperpanchromatic</source>
        <translation type="vanished">Hyper-panchromatique</translation>
    </message>
    <message>
        <source>Orthochromatic</source>
        <translation type="vanished">Orthochromatique</translation>
    </message>
    <message>
        <source>Normal contrast</source>
        <translation type="vanished">Contraste normal</translation>
    </message>
    <message>
        <source>High contrast</source>
        <translation type="vanished">Fort contraste</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="vanished">Paysage</translation>
    </message>
    <message>
        <source>Face in interior</source>
        <translation type="vanished">Visage en intérieur</translation>
    </message>
    <message>
        <source>Channelmixer</source>
        <translation type="vanished">Mixeur de canaux</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="vanished">Rouge</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="vanished">Jaune</translation>
    </message>
    <message>
        <source>Lime</source>
        <translation type="vanished">Lime</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="394"/>
        <source>No flip</source>
        <translation>Aucun retournement</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="395"/>
        <source>Horizontal flip</source>
        <translation>Retournement horizontal</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="396"/>
        <source>Vertical flip</source>
        <translation>Retournement vertical</translation>
    </message>
    <message>
        <source>Soft gaussian</source>
        <translation type="vanished">Gaussien doux</translation>
    </message>
    <message>
        <source>Soft uniform</source>
        <translation type="vanished">Uniforme doux</translation>
    </message>
    <message>
        <source>Soft salt&apos;n pepper</source>
        <translation type="vanished">Poivre et sel doux</translation>
    </message>
    <message>
        <source>Hard gaussian</source>
        <translation type="vanished">Gaussien dur</translation>
    </message>
    <message>
        <source>Hard uniform</source>
        <translation type="vanished">Uniforme dur</translation>
    </message>
    <message>
        <source>Hard salt&apos;n pepper</source>
        <translation type="vanished">Poivre et sel dur</translation>
    </message>
    <message>
        <source>Midtones - Screen</source>
        <translation type="vanished">Tons moyens - Écran</translation>
    </message>
    <message>
        <source>Midtones - Multiply</source>
        <translation type="vanished">Tons moyens - Multiplier</translation>
    </message>
    <message>
        <source>Midtones - Gamma bright</source>
        <translation type="vanished">Tons moyens - Gamma clair</translation>
    </message>
    <message>
        <source>Midtones - Gamma dark</source>
        <translation type="vanished">Tons moyens - Gamma sombre</translation>
    </message>
    <message>
        <source>SoftLight</source>
        <translation type="vanished">Lumière douce</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="58"/>
        <source>Multiply</source>
        <translation>Multiplier</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="59"/>
        <source>Screen</source>
        <translation>Écran</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="60"/>
        <source>Gamma dark</source>
        <translation>Gamma sombre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="61"/>
        <source>Gamma bright</source>
        <translation>Gamma clair</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="62"/>
        <source>Color burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="63"/>
        <source>Color dodge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="65"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="66"/>
        <source>Show Mask</source>
        <translation>Afficher le masque</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="73"/>
        <source>Full image</source>
        <translation>Image complète</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="74"/>
        <source>Vignette</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="75"/>
        <source>Inverted vignette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inv vignette</source>
        <translation type="vanished">Vignette inversée</translation>
    </message>
    <message>
        <source>Green - yellow</source>
        <translation type="vanished">Vert - jaune</translation>
    </message>
    <message>
        <source>Green - cyan</source>
        <translation type="vanished">Vert - cyan</translation>
    </message>
    <message>
        <source>Red - yellow</source>
        <translation type="vanished">Rouge - jaune</translation>
    </message>
    <message>
        <source>Red - magenta</source>
        <translation type="vanished">Rouge - mangenta</translation>
    </message>
    <message>
        <source>Blue - cyan</source>
        <translation type="vanished">Bleu - cyan</translation>
    </message>
    <message>
        <source>Blue - magenta</source>
        <translation type="vanished">Bleu - mangenta</translation>
    </message>
    <message>
        <source>Soft</source>
        <translation type="vanished">Doux</translation>
    </message>
    <message>
        <source>Hard</source>
        <translation type="vanished">Dur</translation>
    </message>
    <message>
        <source>Fancy</source>
        <translation type="vanished">Décoratif</translation>
    </message>
    <message>
        <source>Lighten</source>
        <translation type="vanished">Éclaircir</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="57"/>
        <source>Softlight</source>
        <translation>Lumière douce</translation>
    </message>
    <message>
        <source>Orton screen</source>
        <translation type="vanished">Effet Orton</translation>
    </message>
    <message>
        <source>Orton softlight</source>
        <translation type="vanished">Lumière douce d&apos;Orton</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="36"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="42"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="401"/>
        <source>Only final run</source>
        <translation>Exécution finale seulement</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="402"/>
        <source>With Preview</source>
        <translation>Avec aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="44"/>
        <source>Show mask</source>
        <translation>Afficher le masque</translation>
    </message>
    <message>
        <source>Ratio</source>
        <translation type="vanished">Rapport</translation>
    </message>
    <message>
        <source>Film curve</source>
        <translation type="vanished">Courbe de film</translation>
    </message>
    <message>
        <source>Like UFRaw</source>
        <translation type="vanished">Comme UFraw</translation>
    </message>
    <message>
        <source>Null</source>
        <translation type="vanished">Nul</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="425"/>
        <source>RGB</source>
        <translation>RVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="426"/>
        <source>Structure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="427"/>
        <source>L*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="428"/>
        <source>a*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="429"/>
        <source>b*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="430"/>
        <source>Gradient</source>
        <translation></translation>
    </message>
    <message>
        <source>Structure on L</source>
        <translation type="vanished">Structure sur L</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="434"/>
        <source>PPM 8-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="435"/>
        <source>PPM 16-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="436"/>
        <source>TIFF 8-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="437"/>
        <source>TIFF 16-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="438"/>
        <source>JPEG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="439"/>
        <source>PNG 8-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="440"/>
        <source>PNG 16-bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="444"/>
        <source>4:4:4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="445"/>
        <source>4:2:2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="449"/>
        <source>Full size</source>
        <translation>Pleine taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="450"/>
        <source>Pipe size</source>
        <translation>Taille du tube</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="451"/>
        <source>Only jobfile</source>
        <translation>Fichier de tâches seul</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="452"/>
        <source>Only settings</source>
        <translation>Configuration seule</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="453"/>
        <source>Send to batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="457"/>
        <source>Neutral reset</source>
        <translation>Remise à zéro neutre</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="458"/>
        <source>User reset</source>
        <translation>Remise à zéro utilisateur</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="459"/>
        <location filename="../Sources/ptMain.cpp" line="3859"/>
        <source>Open preset</source>
        <translation>Ouvrir un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="460"/>
        <source>Open settings</source>
        <translation>Ouvrir une configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="465"/>
        <location filename="../Sources/ptGuiOptions.cpp" line="470"/>
        <source>Load one</source>
        <translation>Charger</translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="474"/>
        <source>Clone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGuiOptions.cpp" line="475"/>
        <source>Heal (Dummy)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syntax: photivo [-i imagefile | -j jobfile | -g imagefile] [h] [--new-instance]
Options:
-i imagefile      Specify image file to load. The -i is optional. Starting
                  Photivo with just a file name works the same.
-j jobfile        Specify jobfile for batch processing. Job files are created
                  in Photivo and then executed with this option.
-g imagefile      Specify temporary file used for Gimp-to-Photivo export.
                  Internal option, not intended for general use.
                  BEWARE! This option deletes imagefile!
--new-instance    Allow opening another Photivo instance instead of using a
                  currently running Photivo. Job files are always opened in a
                  new instance.
-h                Display this usage information.

For more documentation visit the wiki:
https://photivo.org
</source>
        <translation type="obsolete">Syntaxe: photivo [-i fichier_image | -j fichier_de_taches | -g fichier_image] [h] [--new-instance]
Options:
-i fichier_image      Spécifie le fichier image à charger. Le -i est optionnel. Démarrer
                  Photivo avec seulement un nom de fichier donne le même résultat.
-j fichier_de_taches        Spécifie un fichier de tâches pour le traitement par lot. Les fichiers  
                  de tâches sont créés dans Photivo puis exécutés avec cette option.
-g fichier_image      Spécifie un fichier temporaire pour l&apos;exportation Photivo vers Gimp.
                  Option interne, non censée être utilisée pour un usage général.
                  ATTENTION! Cette option supprime le fichier image !
--new-instance    Permet d&apos;ouvrir une autre instance de Photivo au lieu d&apos;utiliser l&apos;instance en 
                  cours d&apos;exécution. Les fichiers de tâches sont toujours ouverts dans une nouvelle 
                  instance
-h                Affiche l&apos;aide.

Pour plus d&apos;informations, visitez le wiki :
https://photivo.org
</translation>
    </message>
    <message>
        <source>Photivo command line options</source>
        <translation type="obsolete">Options de la ligne de commande de Photivo</translation>
    </message>
    <message>
        <source>Unrecognized command line options</source>
        <translation type="obsolete">Options de la ligne de commande non reconnues</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="615"/>
        <location filename="../Sources/ptMain.cpp" line="926"/>
        <source>Photivo</source>
        <translation></translation>
    </message>
    <message>
        <source>Loading curves (</source>
        <translation type="obsolete">Chargement des courbes (</translation>
    </message>
    <message>
        <source>Cannot read curve </source>
        <translation type="obsolete">Impossile de lire la courbe</translation>
    </message>
    <message>
        <source>Curve read error</source>
        <translation type="obsolete">Erreur de lecture de la courbe</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1327"/>
        <location filename="../Sources/ptMain.cpp" line="1489"/>
        <location filename="../Sources/ptMain.cpp" line="1527"/>
        <location filename="../Sources/ptMain.cpp" line="1755"/>
        <location filename="../Sources/ptMain.cpp" line="2084"/>
        <location filename="../Sources/ptMain.cpp" line="2358"/>
        <location filename="../Sources/ptMain.cpp" line="3011"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Loading channelmixers</source>
        <translation type="vanished">Chargement des mixeurs de canaux</translation>
    </message>
    <message>
        <source>Cannot read channelmixer </source>
        <translation type="vanished">Impossible de lire le mixeur de canaux</translation>
    </message>
    <message>
        <source>Channelmixer read error</source>
        <translation type="vanished">Erreur de lecture du mixeur de canaux</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1516"/>
        <source>Selection too small</source>
        <translation>Sélection trop petite</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1517"/>
        <source>Selection rectangle needs to be at least 50x50 pixels in size.
No crop, try again.</source>
        <translation>Le rectangle de découpe doit mesurer au moins 50x50 pixels.
Découpage non effectué. Essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1487"/>
        <location filename="../Sources/ptMain.cpp" line="1525"/>
        <location filename="../Sources/ptMain.cpp" line="1844"/>
        <location filename="../Sources/ptMain.cpp" line="1916"/>
        <source>Updating histogram</source>
        <translation>Mise à jour de l&apos;histogramme</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="547"/>
        <source>Photivo crashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="548"/>
        <source>Photivo crashed. You can get help on our flickr forum at
&lt;a href=&quot;https://www.flickr.com/groups/photivo/discuss/&quot;&gt;https://www.flickr.com/groups/photivo/discuss/&lt;/a&gt;
When you post there make sure to describe your last actions before the crash occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="561"/>
        <source>Fatal error: Wrong GraphicsMagick quantum depth!
Found quantum depth %1. Photivo needs at least %2.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="566"/>
        <source>Photivo: Fatal Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="572"/>
        <source>Syntax: photivo [inputfile | -i imagefile | -j jobfile |
                 --load-and-delete imagefile]
                [--pts ptsfile] [--sidecar sidecarfile] [-h] [--new-instance]
Options:
inputfile
      Specify the image or settings file to load. Works like -i for image files
      and like --pts for settings files.
-i imagefile
      Specify image file to load.
-j jobfile
      Specify jobfile for batch processing. Job files are created
 in Photivo
      and then executed with this option.
--load-and-delete imagefile
      Specify temporary file used for Gimp-to-Photivo export. Internal option,
      not intended for general use. BEWARE! This option deletes imagefile!
--pts ptsfile
      Specify settings file to load with the image. Must be used together
      with -i.
--sidecar sidecarfile
      Specify sidecar file to load with the image.
--new-instance
      Allow opening another Photivo instance instead of using a currently
      running Photivo. Job files are always opened in a new instance.
--no-fmgr or -p
      Prevent auto-open file manager when Photivo starts.
--help or -h
      Display this usage information.

For more documentation visit the wiki: https://photivo.org
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1075"/>
        <source>Clean up input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1483"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1556"/>
        <location filename="../Sources/ptMain.cpp" line="1612"/>
        <source>WebResizing</source>
        <translation>Redimensionnement Web</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1570"/>
        <source>Applying base curve</source>
        <translation>Application de la courbe de base</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1576"/>
        <source>Applying gamma compensation</source>
        <translation>Application de la compensation du gamma</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1597"/>
        <source>Applying RGB Contrast</source>
        <translation>Application du contraste RVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1604"/>
        <source>Applying after gamma curve</source>
        <translation>Application de la courbe post-gamma</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1623"/>
        <source>Wiener Filter</source>
        <translation>Filtre de Wiener</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1760"/>
        <source>Updating preview image</source>
        <translation>Mise à jour de l&apos;aperçu d&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1857"/>
        <location filename="../Sources/ptMain.cpp" line="1976"/>
        <location filename="../Sources/ptMain.cpp" line="2041"/>
        <source>Histogram selection outside the image</source>
        <translation>Sélection pour l&apos;histogramme hors de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1858"/>
        <location filename="../Sources/ptMain.cpp" line="1977"/>
        <location filename="../Sources/ptMain.cpp" line="2042"/>
        <source>Histogram selection rectangle too large.
No crop, try again.</source>
        <translation>Sélection pour l&apos;histogramme trop grande.
Découpage non effectué, essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1881"/>
        <source>Indicating exposure</source>
        <translation>Indication de l&apos;exposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1923"/>
        <location filename="../Sources/ptMain.cpp" line="2284"/>
        <location filename="../Sources/ptMain.cpp" line="2887"/>
        <source>Converting to output space</source>
        <translation>Conversion vers l&apos;espace de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="1994"/>
        <source>Converting to screen space</source>
        <translation>Conversion vers l&apos;espace d&apos;affichage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2130"/>
        <location filename="../Sources/ptMain.cpp" line="2579"/>
        <source>Cannot decode</source>
        <translation>Décodage impossible</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2210"/>
        <source>Memory error, no conversion for file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2312"/>
        <source>Writing output</source>
        <translation>Écriture de la sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2324"/>
        <source>GraphicsMagick Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2324"/>
        <source>No output file written.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2326"/>
        <source>Writing output (exif)</source>
        <translation>Écriture de la sortie (exif)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2333"/>
        <source>Exif Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2333"/>
        <source>No exif data written.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2342"/>
        <source>Writing output (settings)</source>
        <translation>Écriture de la sortie en (configuration)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2353"/>
        <source>Written %L1 bytes (%L2 MByte)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2380"/>
        <location filename="../Sources/ptMain.cpp" line="2717"/>
        <source>Jpg images (*.jpg *.jpeg);;All files (*.*)</source>
        <translation>Images Jpg (*.jpg *.jpeg);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2385"/>
        <location filename="../Sources/ptMain.cpp" line="2722"/>
        <source>PNG images(*.png);;All files (*.*)</source>
        <translation>Images Png (*.png);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2390"/>
        <location filename="../Sources/ptMain.cpp" line="2727"/>
        <source>Tiff images (*.tif *.tiff);;All files (*.*)</source>
        <translation>Images Tiff (*.tif *.tiff);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2394"/>
        <location filename="../Sources/ptMain.cpp" line="2731"/>
        <source>Ppm images (*.ppm);;All files (*.*)</source>
        <translation>Images Ppm (*.ppm);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3792"/>
        <source>Save and send to batch manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="523"/>
        <location filename="../Sources/ptMain.cpp" line="2401"/>
        <location filename="../Sources/ptMain.cpp" line="2738"/>
        <source>Save File</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="515"/>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="532"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="515"/>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="532"/>
        <source>Thumbnail could not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="536"/>
        <source>Exif error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="536"/>
        <source>Exif data could not be written.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2541"/>
        <source>Open Raw</source>
        <translation>Ouvrir un raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2552"/>
        <source>File not found</source>
        <translation>Fichier introuvable</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2553"/>
        <source>Input file does not exist.</source>
        <translation>Le fichier d&apos;entrée n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2566"/>
        <source>Reading file</source>
        <translation>Lecture du fichier</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="338"/>
        <source>Append settings file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="368"/>
        <source>Select input file(s)</source>
        <translation>Veuillez sélectionner les fichiers d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="380"/>
        <source>Select output directory</source>
        <translation>Veuillez sélectionner le répertoire de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="395"/>
        <source>Select job file</source>
        <translation>Veuillez sélectionner un fichier de tâches</translation>
    </message>
    <message>
        <source>Settings File</source>
        <translation type="obsolete">Fichier de configuration</translation>
    </message>
    <message>
        <source>Cannot exit</source>
        <translation type="obsolete">Impossible de quitter</translation>
    </message>
    <message>
        <source>Please finish your crop before closing Photivo.</source>
        <translation type="obsolete">Veuillez terminer le découpage avant de fermer Photivo.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2855"/>
        <source>Writing tmp image for gimp</source>
        <translation>Écriture de l&apos;image temporaire pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2927"/>
        <source>Writing tmp exif for gimp</source>
        <translation>Écriture de l&apos;exif temporaire pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2941"/>
        <source>Writing tmp icc for gimp</source>
        <translation>Écriture du fichier icc temporaire pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="2974"/>
        <source>Writing gimp interface file</source>
        <translation>Écriture du fichier d&apos;interface pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3136"/>
        <location filename="../Sources/ptMain.cpp" line="3137"/>
        <source>Please load a profile first</source>
        <translation>Veuillez tout d&apos;abord charger un profil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3145"/>
        <source>Not yet implemented</source>
        <translation>Pas encore implémenté</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3146"/>
        <source>Not yet implemented. Reverting to Adobe.</source>
        <translation>Pas encore implémenté. Retour à Adobe.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3156"/>
        <location filename="../Sources/ptMain.cpp" line="3199"/>
        <location filename="../Sources/ptMain.cpp" line="3239"/>
        <source>Open Profile</source>
        <translation>Ouvrir un profil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3345"/>
        <source>Open Image</source>
        <translation>Ouvrir une image</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3347"/>
        <source>CSS files (*.css *.qss);;All files(*.*)</source>
        <translation>Fichier CSS (*.css *.qss);;Tous les fichiers(*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3386"/>
        <source>Are you sure?</source>
        <translation>Le souhaitez-vous vraiment ?</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3387"/>
        <source>If you don&apos;t stop me, I will waste %1 MB of memory.</source>
        <translation>Si vous ne m&apos;arrêtez pas, je vais perdre %1 Mo de mémoire.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3408"/>
        <source>Really switch to 1:1 pipe?</source>
        <translation>Souhaitez-vous vraiment basculer vers le tube 1:1 ?</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3409"/>
        <source>Switching to 1:1 pipe will increase memory usage and processing time greatly.
Are you sure?</source>
        <translation>Basculer vers le tube 1:1 va grandement augmenter l&apos;utilisation mémoire et le temps de traitement.
Le souhaitez-vous vraiment ?</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3411"/>
        <location filename="../Sources/ptMain.cpp" line="3448"/>
        <source>Detail view</source>
        <translation>Vue détaillée</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3580"/>
        <location filename="../Sources/ptMain.cpp" line="3595"/>
        <source>Reset?</source>
        <translation>Remettre à zéro ?</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3581"/>
        <source>Reset to neutral values?
</source>
        <translation>Revenir à des valeurs neutre ?
</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3596"/>
        <source>Reset to start up settings?
</source>
        <translation>Revenir à la configuration de démarrage ?</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3625"/>
        <source>Get Gimp command</source>
        <translation>Récupérer la commande pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3656"/>
        <source>Get preset file</source>
        <translation>Récupérer un fichier de préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3784"/>
        <source>Save full size image</source>
        <translation>Enregistrer l&apos;image en pleine taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3786"/>
        <source>Save current pipe</source>
        <translation>Enregistrer le tube courant</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3788"/>
        <source>Save job file</source>
        <translation>Enregistrer un fichier de tâches</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="339"/>
        <location filename="../Sources/filters/ptFilterDM.cpp" line="414"/>
        <location filename="../Sources/ptMain.cpp" line="3790"/>
        <source>Save settings file</source>
        <translation>Enregistrer un fichier de configuration</translation>
    </message>
    <message>
        <source>Settings files (*.pts *.ptj);;All files (*.*)</source>
        <translation type="vanished">Fichiers de configuration (*.pts *.ptj);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="129"/>
        <location filename="../Sources/ptMain.cpp" line="3842"/>
        <location filename="../Sources/ptMain.cpp" line="3856"/>
        <source>All supported files (*.pts *ptj);;Settings files (*.pts);;Job files (*.ptj);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="132"/>
        <source>Open setting files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3845"/>
        <source>Open setting file</source>
        <translation>Ouvrir un fichier de configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3882"/>
        <source>Open &apos;bad pixels&apos; file</source>
        <translation>Ouvrir un fichier de &quot;pixels morts&quot;</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3906"/>
        <source>Open &apos;dark frame&apos; file</source>
        <translation>Ouvrir une image noire (dark)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="3954"/>
        <location filename="../Sources/ptMain.cpp" line="3955"/>
        <source>Spot WB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4394"/>
        <source>No selection</source>
        <translation>Aucune sélection</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4395"/>
        <location filename="../Sources/ptMain.cpp" line="4594"/>
        <source>Open an image first.</source>
        <translation>Veuillez tout-d&apos;abord ouvrir une image.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4407"/>
        <location filename="../Sources/ptMain.cpp" line="4408"/>
        <source>Get angle</source>
        <translation>Prenez l&apos;angle</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4593"/>
        <source>No crop possible</source>
        <translation>Pas de découpage possible</translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="201"/>
        <location filename="../Sources/ptMain.cpp" line="4598"/>
        <source>Prepare</source>
        <translation>Préparer</translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="202"/>
        <source>Prepare for local adjust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="207"/>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="208"/>
        <source>Local adjust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4599"/>
        <source>Prepare for cropping</source>
        <translation>Préparer pour le découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4607"/>
        <location filename="../Sources/ptMain.cpp" line="4608"/>
        <source>Crop</source>
        <translation>Découper</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4642"/>
        <source>Crop too small</source>
        <translation>Découpage trop petit</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4643"/>
        <source>Crop rectangle needs to be at least 4x4 pixels in size.
No crop, try again.</source>
        <translation>Le rectangle de découpe doit mesurer au moins 4x4 pixels.
Découpage non effectué. Essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4701"/>
        <source>No previous crop found</source>
        <translation>Aucun découpage antérieur trouvé</translation>
    </message>
    <message>
        <location filename="../Sources/ptMain.cpp" line="4702"/>
        <source>Set a crop rectangle now.</source>
        <translation>Veuillez définir le rectangle de découpe.</translation>
    </message>
    <message>
        <source>Open Channelmixer</source>
        <translation type="vanished">Ouvrir le mixeur de canaux</translation>
    </message>
    <message>
        <source>Save Channelmixer</source>
        <translation type="vanished">Enregistrer le mixeur de canaux</translation>
    </message>
    <message>
        <source>Give a descridlion</source>
        <translation type="obsolete">Donner une description</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="176"/>
        <source>Open Curve</source>
        <translation type="unfinished">Ouvrir une courbe</translation>
    </message>
    <message>
        <source>This curve is meant for channel </source>
        <translation type="obsolete">Cette courbe est faite pour un canal</translation>
    </message>
    <message>
        <source>. Continue anyway ?</source>
        <translation type="obsolete">. Continuer quand même ?</translation>
    </message>
    <message>
        <source>Incompatible curve</source>
        <translation type="obsolete">Courbe incompatible</translation>
    </message>
    <message>
        <source>Save Curve</source>
        <translation type="obsolete">Enregistrer la courbe</translation>
    </message>
    <message>
        <source>Get texture bitmap file</source>
        <translation type="vanished">Récupérer un fichier bitmap de texture</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="52"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="74"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="100"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="130"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="156"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="182"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="234"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="264"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="294"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="324"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="372"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="398"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="424"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="450"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="480"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="510"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="540"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="600"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="652"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="694"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="726"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="778"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="830"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="882"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="942"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="994"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1046"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1072"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1202"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1254"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1306"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1332"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1384"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1410"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1436"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1462"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1488"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1514"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1540"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1592"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1618"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1644"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1726"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1748"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1774"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1800"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1826"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1852"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1878"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1904"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1938"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1982"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2034"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2060"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2086"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2190"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2370"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2396"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2418"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2452"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3058"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3094"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3134"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3186"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3230"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3256"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5738"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5798"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5878"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5978"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5992"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6250"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6782"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6796"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6826"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6886"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6958"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6988"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7246"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7344"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7408"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7942"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8196"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8218"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8270"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8360"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8400"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8604"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8672"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8818"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8832"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8850"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8962"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9030"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9064"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9132"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9166"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9200"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9274"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9312"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9346"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9384"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9440"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9550"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9580"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9606"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9636"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9730"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9782"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9834"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9886"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9938"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9964"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9990"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10016"/>
        <source>Daylight</source>
        <translation>Lumière du jour</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="56"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="78"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="104"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="134"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="186"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="238"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="268"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="298"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="328"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="376"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="402"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="428"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="454"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="484"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="514"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="544"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="604"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="656"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="686"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="734"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="786"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="838"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="950"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1002"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1054"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1132"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1158"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1184"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1210"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1262"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1340"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1366"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1392"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1418"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1496"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1522"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1548"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1600"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1652"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1678"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1722"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1882"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1908"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1946"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2460"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2486"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3070"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3128"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3146"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3190"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3238"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3264"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3602"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3628"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3654"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3680"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3706"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3848"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4050"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4200"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4516"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4542"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4858"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4908"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5086"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5182"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5266"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5496"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5546"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5596"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5750"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5776"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5794"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5832"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5874"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5982"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6258"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6786"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6800"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6830"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6966"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7254"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7322"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7352"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7382"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7416"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7442"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7950"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8200"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8222"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8252"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8274"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8364"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8382"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8404"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8448"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8608"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8676"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8822"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8836"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8854"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8970"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9038"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9072"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9140"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9174"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9282"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9320"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9392"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9448"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9554"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9588"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9614"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9644"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9738"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9842"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9946"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9998"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10024"/>
        <source>Cloudy</source>
        <translation>Nuageux</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="60"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="82"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="108"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="190"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="242"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="272"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="302"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="332"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="380"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="406"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="432"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="458"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="488"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="518"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="548"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="608"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="660"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="682"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="738"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="842"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="954"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="980"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1006"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1032"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1058"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1084"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1110"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1136"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1162"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1188"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1214"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1266"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1344"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1370"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1396"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1448"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1526"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1552"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1604"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1656"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1682"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1742"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2386"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2400"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2440"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3088"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3180"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3194"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3242"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3268"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5906"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7954"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8918"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8952"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8986"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9054"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9088"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9122"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9156"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9190"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9302"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9336"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9412"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9464"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9498"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9528"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9592"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9618"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9648"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9742"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9794"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9846"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10028"/>
        <source>Tungsten</source>
        <translation>Tungstène</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="64"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="86"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="194"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="246"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="276"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="306"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="336"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="384"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="410"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="436"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="462"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="492"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="522"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="552"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="664"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="678"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="742"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="794"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="846"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="958"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="984"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1010"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1036"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1062"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1088"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1192"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1218"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1270"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1322"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1530"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1556"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1660"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1686"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1734"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1934"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2390"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2404"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2684"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3066"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3084"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3172"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3198"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3246"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3272"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3616"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3668"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3694"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4188"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4530"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5100"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5126"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5152"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5254"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5746"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6266"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7330"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7424"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7958"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8260"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9596"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9622"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9652"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9678"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9746"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9798"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9824"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9850"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9954"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9980"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10006"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10032"/>
        <source>Fluorescent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="68"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="146"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="172"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="198"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="250"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="310"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="340"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="388"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="440"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="466"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="496"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="526"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="556"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="616"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="642"/>
        <source>FluorescentHigh</source>
        <translation>Fluorescent élevé</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="90"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1782"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1834"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1886"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1964"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1990"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2042"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2068"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2094"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2120"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2146"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2172"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2198"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2448"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3598"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4034"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5214"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5338"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5528"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5628"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5678"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5728"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5806"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5974"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6528"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6778"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6850"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6952"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6982"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7270"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7368"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7398"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7458"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8238"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8906"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8974"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9042"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9110"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9144"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9178"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9286"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9324"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9396"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9452"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9486"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9524"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9724"/>
        <source>DaylightFluorescent</source>
        <translation>Fluorescent lumière du jour</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="94"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="120"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="258"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="348"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="534"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="564"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6270"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7334"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7962"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8264"/>
        <source>Underwater</source>
        <translation>Sous-marin</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="202"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="254"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="344"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="366"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="392"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="418"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="530"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="560"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="620"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="668"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="746"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="798"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="824"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="850"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="962"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="988"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1014"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1040"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1066"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1092"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1118"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1144"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1170"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1196"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1222"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1248"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1274"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1326"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1352"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1404"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1456"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1534"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1560"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1664"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1690"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1730"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1942"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2456"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3154"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3202"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3250"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3276"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3606"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3624"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3650"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3676"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3702"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4046"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4196"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4370"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4538"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4854"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4904"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5082"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5108"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5134"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5178"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5262"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5350"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5492"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5542"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5592"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5754"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5780"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6274"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6532"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6910"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7274"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7308"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7338"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7402"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7428"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7462"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7966"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8286"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8308"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8330"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8372"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8394"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8416"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8438"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8460"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8598"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8620"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8862"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8884"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8922"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8990"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9058"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9092"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9126"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9194"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9306"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9340"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9416"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9468"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9502"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9532"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9600"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9656"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9682"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9750"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9776"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9802"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9828"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9854"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9906"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9958"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9984"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10010"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10036"/>
        <source>Flash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="690"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="730"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="782"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="834"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="886"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="946"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="998"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1050"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1128"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1154"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1180"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1206"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1258"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1310"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1336"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1388"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1440"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1466"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1492"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1518"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1544"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1596"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1622"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1648"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1738"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1778"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1830"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1950"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1960"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1986"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2038"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2064"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2090"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2194"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2408"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2464"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2490"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3234"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3260"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3632"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3658"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3684"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3710"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3852"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4054"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4204"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4520"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4546"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4862"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5090"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5186"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5270"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5550"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5600"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5650"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5758"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5784"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5828"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5870"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5986"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6254"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6834"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6962"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6992"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7250"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7348"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7412"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7438"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7946"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8248"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8278"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8322"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8386"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8408"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8452"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8680"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8858"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8966"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9034"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9068"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9136"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9170"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9204"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9278"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9316"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9350"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9388"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9584"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9610"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9640"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9666"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9734"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9786"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9838"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9942"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9994"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10020"/>
        <source>Shade</source>
        <translation>Ombre</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1114"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1140"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1166"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1348"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1400"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1452"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1608"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3120"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4022"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4346"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4878"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5202"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5326"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5516"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5616"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5666"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5818"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5970"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6520"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6770"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6842"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6902"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6974"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7262"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7360"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7390"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7450"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8230"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8914"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8982"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9050"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9084"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9118"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9152"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9186"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9332"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9400"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9460"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9494"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9516"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9558"/>
        <source>WhiteFluorescent</source>
        <translation>Fluorescent blanc</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="906"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2324"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2800"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3380"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3794"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4442"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5442"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6156"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6410"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6668"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7152"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7598"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7848"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8342"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8528"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8658"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9248"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9536"/>
        <source>5000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="910"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3440"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4462"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8548"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8666"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8788"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9544"/>
        <source>6500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1786"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1838"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1890"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1994"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2046"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2072"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2202"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4018"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4342"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4684"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4874"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5198"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5322"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5562"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9298"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9370"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9408"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9716"/>
        <source>WarmWhiteFluorescent</source>
        <translation>Fluorescent blanc chaud</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1842"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1998"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2050"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2128"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2154"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2180"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2206"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4026"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4350"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4882"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5074"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5206"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5330"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5520"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5620"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5814"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5852"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9294"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9366"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9404"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9708"/>
        <source>CoolWhiteFluorescent</source>
        <translation>Fluorescent blanc froid</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1794"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1846"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1930"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="1976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2002"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2054"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2132"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2158"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2184"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2210"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2680"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3062"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3664"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3690"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4010"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4184"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4334"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4526"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4676"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4866"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5070"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5096"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5122"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5148"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5190"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5250"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5276"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5554"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5604"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5654"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5742"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5822"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5962"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6262"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6516"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6762"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6838"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6898"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6970"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7258"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7326"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7356"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7386"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7420"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7446"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8226"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8256"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8282"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8326"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8368"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8390"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8412"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8456"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8616"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8684"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8826"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8866"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8888"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9950"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="10002"/>
        <source>Incandescent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2382"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5802"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5882"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8204"/>
        <source>EveningSun</source>
        <translation>Lumière crépusculaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2494"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4386"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6072"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6326"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6584"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7068"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7514"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8018"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8468"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8696"/>
        <source>2600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2256"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2498"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3726"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4070"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4390"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4562"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4736"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4960"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6334"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6592"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7522"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8026"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8472"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8700"/>
        <source>2700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2260"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2502"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3730"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4394"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6088"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6342"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6600"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7084"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7530"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7780"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8034"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8476"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8704"/>
        <source>2800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2506"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4398"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6092"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6346"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6604"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7088"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7534"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7784"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8038"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8480"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8708"/>
        <source>2900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2272"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2510"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2952"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3742"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3892"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4402"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6096"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6350"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6608"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7092"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7538"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7788"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8042"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8334"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8484"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8650"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9240"/>
        <source>3000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2276"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2514"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2724"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3746"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6100"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6612"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7096"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7542"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7792"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8046"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8488"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8716"/>
        <source>3100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2518"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2728"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2960"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3308"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3750"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4406"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6104"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6358"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6616"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7100"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7546"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7796"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8050"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8492"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8720"/>
        <source>3200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2522"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2732"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2964"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3312"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3754"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6108"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6620"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7104"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7550"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7800"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8054"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8496"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8654"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8724"/>
        <source>3300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2288"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2526"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2736"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3316"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3758"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4410"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6366"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6624"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7108"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7554"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8058"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8728"/>
        <source>3400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2530"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2740"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3320"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6370"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6628"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7112"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7558"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8062"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8504"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8732"/>
        <source>3500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2534"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2744"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3324"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3762"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4598"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6120"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6374"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6632"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7116"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7562"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8066"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8736"/>
        <source>3600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2538"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2748"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3328"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3766"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4110"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4260"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4602"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4776"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6378"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6636"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7120"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8070"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8508"/>
        <source>3700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2542"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2980"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3332"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3770"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4418"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4606"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6128"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6382"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6640"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7124"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8074"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8740"/>
        <source>3800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2546"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3336"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6132"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6386"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6644"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7128"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7824"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8078"/>
        <source>3900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2550"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2984"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3340"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3774"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4118"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4268"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4610"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4784"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6136"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6390"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6648"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7132"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7828"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8082"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8338"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8744"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9244"/>
        <source>4000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2554"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3344"/>
        <source>4100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2308"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2558"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2988"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3348"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3778"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4614"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6140"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6394"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6652"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7136"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7832"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8086"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8516"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8748"/>
        <source>4200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2312"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2562"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3352"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3782"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4618"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5952"/>
        <source>4300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2566"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2776"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2992"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3356"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6144"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6398"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6656"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7140"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7836"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8090"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8752"/>
        <source>4400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2316"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2570"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2780"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3360"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3786"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4622"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8520"/>
        <source>4500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2784"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3364"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6148"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6402"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6660"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7144"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7590"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8094"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8756"/>
        <source>4600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2578"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2788"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3368"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8524"/>
        <source>4700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2320"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2792"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3372"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3790"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4438"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6152"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6406"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6664"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7148"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8760"/>
        <source>4800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2586"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2796"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3376"/>
        <source>4900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2804"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3388"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3952"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6414"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6672"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7156"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7602"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7852"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8532"/>
        <source>5200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2598"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3396"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6418"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6676"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7160"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7606"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8110"/>
        <source>5400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2332"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2602"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3404"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3802"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4450"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6422"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6680"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7164"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7610"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8114"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9630"/>
        <source>5600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2606"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3412"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6172"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6426"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6684"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7168"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7614"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8118"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8776"/>
        <source>5800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2610"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3420"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6430"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7172"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7618"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8122"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8346"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8544"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8780"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9252"/>
        <source>6000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2614"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2824"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3428"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4458"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6180"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6434"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7176"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7622"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8126"/>
        <source>6200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2618"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2828"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3436"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6184"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6438"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7180"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8130"/>
        <source>6400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2622"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2832"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3444"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6188"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6442"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6700"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7184"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8134"/>
        <source>6600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2626"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2836"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3032"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3452"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4466"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6192"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6446"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7188"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7884"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8138"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8792"/>
        <source>6800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2630"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3460"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6196"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6450"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6918"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7192"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7888"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8350"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8552"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9256"/>
        <source>7000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3468"/>
        <source>7200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2848"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3476"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6200"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6454"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6712"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7196"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7892"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8146"/>
        <source>7400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2852"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3484"/>
        <source>7600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2856"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3040"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3492"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6204"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6458"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7200"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7896"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8800"/>
        <source>7800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2650"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2860"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3500"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3992"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8560"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9260"/>
        <source>8000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2356"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2654"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3044"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3512"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3826"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8804"/>
        <source>8300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2658"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3524"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6466"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6724"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7654"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7904"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8158"/>
        <source>8600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3536"/>
        <source>8900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2666"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3548"/>
        <source>9200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3560"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4490"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8572"/>
        <source>9500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2884"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3572"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6736"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7666"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8170"/>
        <source>9800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2906"/>
        <source>BlackNWhite</source>
        <translation>Noir et Blanc</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3714"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4058"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4382"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4550"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4724"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6064"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6576"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7060"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7506"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8010"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8464"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8692"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9236"/>
        <source>2500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2328"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3392"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3798"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4446"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4634"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8768"/>
        <source>5300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3400"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8536"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9540"/>
        <source>5500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2340"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3432"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3810"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8784"/>
        <source>6300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3036"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3472"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8796"/>
        <source>7300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3048"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3540"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4486"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6728"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7658"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7908"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8162"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8568"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9264"/>
        <source>9000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2364"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3052"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3834"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4178"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4328"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4494"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4844"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5064"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6740"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7670"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8174"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8576"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9268"/>
        <source>10000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3384"/>
        <source>5100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3408"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8540"/>
        <source>5700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2336"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3416"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3806"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4454"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4642"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5308"/>
        <source>5900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3424"/>
        <source>6100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2344"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3448"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3814"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4650"/>
        <source>6700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3456"/>
        <source>6900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2348"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3464"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3818"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4654"/>
        <source>7100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3480"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8556"/>
        <source>7500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2352"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3488"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3822"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4658"/>
        <source>7700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3496"/>
        <source>7900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3504"/>
        <source>8100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6208"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6462"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7204"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7650"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7900"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8154"/>
        <source>8200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3516"/>
        <source>8400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3520"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8564"/>
        <source>8500K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3528"/>
        <source>8700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3532"/>
        <source>8800K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2360"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3544"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3830"/>
        <source>9100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3552"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4666"/>
        <source>9300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3556"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6220"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6732"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7662"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8166"/>
        <source>9400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3564"/>
        <source>9600K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3568"/>
        <source>9700K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3576"/>
        <source>9900K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3620"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3646"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3672"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3698"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4042"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4192"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4366"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4534"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4708"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4850"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4900"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5078"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5104"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5130"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5156"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5174"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5258"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5346"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5488"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5538"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5588"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5638"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5688"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5836"/>
        <source>DirectSunlight</source>
        <translation>Soleil direct</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2248"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3718"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4554"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6068"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6322"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6580"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7064"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7510"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8014"/>
        <source>2550K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2252"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3722"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4558"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6076"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6330"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6588"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7072"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7518"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8022"/>
        <source>2650K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2264"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3734"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4570"/>
        <source>2850K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="2268"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3738"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4574"/>
        <source>2950K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3868"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4062"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4212"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4728"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4952"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5366"/>
        <source>2560K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3872"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4066"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4216"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4732"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5370"/>
        <source>2630K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3880"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4074"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4224"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4740"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4964"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5378"/>
        <source>2780K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3884"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4078"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4744"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4968"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5382"/>
        <source>2860K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3888"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4082"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4748"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5386"/>
        <source>2940K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3896"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4086"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4976"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5390"/>
        <source>3030K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3900"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4090"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4980"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5394"/>
        <source>3130K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3904"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4094"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4760"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4984"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5398"/>
        <source>3230K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3908"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4098"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4248"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4764"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4988"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5402"/>
        <source>3330K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3912"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4102"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4252"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4768"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4992"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5406"/>
        <source>3450K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4106"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4256"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4772"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5410"/>
        <source>3570K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4114"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4264"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4780"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5004"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5418"/>
        <source>3850K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4122"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4272"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4788"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5426"/>
        <source>4170K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4126"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4276"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4792"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5016"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5430"/>
        <source>4350K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3940"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4130"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4280"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4796"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5434"/>
        <source>4550K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4134"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4284"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4800"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5438"/>
        <source>4760K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3956"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4142"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4292"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4808"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5446"/>
        <source>5260K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3960"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4146"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4296"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4812"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5032"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5450"/>
        <source>5560K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3964"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4150"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5036"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5454"/>
        <source>5880K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3972"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4154"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4304"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4820"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5040"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5458"/>
        <source>6250K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3980"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4158"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4308"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4824"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5044"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5462"/>
        <source>6670K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3984"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4162"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4312"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4828"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5048"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5466"/>
        <source>7140K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3988"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4166"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4316"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4832"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5052"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5470"/>
        <source>7690K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3996"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4170"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4320"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4836"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5056"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5474"/>
        <source>8330K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4000"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4174"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4324"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4840"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5060"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5478"/>
        <source>9090K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4014"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4338"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4680"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4870"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4920"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5194"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5318"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5508"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5558"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5608"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5658"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5708"/>
        <source>SodiumVaporFluorescent</source>
        <translation>Fluorescent Vapeur de Sodium</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="3594"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4030"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4354"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4696"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4886"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5210"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5334"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5524"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5574"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5624"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5724"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9720"/>
        <source>DayWhiteFluorescent</source>
        <translation>Fluorescent Blanc Lumière du jour</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4038"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4362"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4704"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="4944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5218"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5342"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5532"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5582"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5632"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5682"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5732"/>
        <source>HighTempMercuryVaporFluorescent</source>
        <translation>Fluorescent Vapeur de mercure haute température</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5810"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5848"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5894"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6524"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6774"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6816"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6846"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6876"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6906"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6948"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6978"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7008"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7266"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7300"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7364"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7394"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7454"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8234"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8910"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8944"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8978"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9012"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9046"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9114"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9148"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9182"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9290"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9328"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9456"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9490"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9520"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="9562"/>
        <source>NeutralFluorescent</source>
        <translation>Fluorescent neutre</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5864"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5886"/>
        <source>FlashAuto</source>
        <translation>Flash Auto</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5916"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="5966"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6766"/>
        <source>IncandescentWarm</source>
        <translation>Incandescent chaud</translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6278"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6536"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6914"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7020"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7466"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7716"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7970"/>
        <source>2000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6282"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6540"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7024"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7470"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7720"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7974"/>
        <source>2050K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6032"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6286"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6544"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7028"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7474"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7724"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7978"/>
        <source>2100K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6036"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6290"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6548"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7032"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7478"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7728"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7982"/>
        <source>2150K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6040"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6294"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6552"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7036"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7482"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7732"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7986"/>
        <source>2200K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6044"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6298"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6556"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7040"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7486"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7736"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7990"/>
        <source>2250K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6048"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6302"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6560"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7044"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7490"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7740"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7994"/>
        <source>2300K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6052"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6306"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6564"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7048"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7494"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7744"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7998"/>
        <source>2350K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6056"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6310"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6568"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7052"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7498"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7748"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8002"/>
        <source>2400K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6060"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6314"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6572"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7056"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7502"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8006"/>
        <source>2450K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6084"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6338"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6596"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7080"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7526"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7776"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8030"/>
        <source>2750K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6486"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6744"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7228"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7674"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7924"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8178"/>
        <source>11000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6490"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6748"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7232"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7678"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7928"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8182"/>
        <source>12000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6494"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6752"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7236"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7682"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7932"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8186"/>
        <source>13000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6244"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6498"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6756"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="6922"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7240"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7686"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="7936"/>
        <location filename="../Sources/ptWhiteBalances.cpp" line="8190"/>
        <source>14000K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="146"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="48"/>
        <source>Photivo: Load configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="52"/>
        <source>Discard current configuration and load new settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="60"/>
        <source>Discard current configuration and reset to startup preset?
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="65"/>
        <source>Discard current configuration and reset to neutral preset?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="69"/>
        <source>Discard current configuration and load preset file?
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="78"/>
        <source>Discard current configuration and load settings file?
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="127"/>
        <source>Photivo: Save image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="128"/>
        <location filename="../Sources/ptConfirmRequest.cpp" line="140"/>
        <source>Do you want to save the current image?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="132"/>
        <source>Photivo: Open image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptConfirmRequest.cpp" line="133"/>
        <source>Before opening the image:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurve.cpp" line="453"/>
        <source>Failed to load curve file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurve.cpp" line="455"/>
        <source>
The error occurred in line %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurve.cpp" line="456"/>
        <source>Load curve file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="471"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="477"/>
        <source>Updating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="483"/>
        <source>Processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="151"/>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="182"/>
        <source>No image opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="152"/>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="183"/>
        <source>Open an image before editing spots.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="75"/>
        <source>Midtones: screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="76"/>
        <source>Midtones: multiply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="77"/>
        <source>Midtones: gamma bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="78"/>
        <source>Midtones: gamma dark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextureOverlayForm</name>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.ui" line="71"/>
        <source>Load overlay image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToneForm</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.ui" line="18"/>
        <source>All values</source>
        <translation type="unfinished">Toutes les valeurs</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.ui" line="62"/>
        <source>Shadows</source>
        <translation type="unfinished">Ombres</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.ui" line="106"/>
        <source>Midtones</source>
        <translation type="unfinished">Tons moyens</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.ui" line="150"/>
        <source>Highlights</source>
        <translation type="unfinished">Hautes lumières</translation>
    </message>
</context>
<context>
    <name>ptBatchWindow</name>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="26"/>
        <source>Add job to list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="29"/>
        <source>Add job...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="52"/>
        <source>Remove jobs from list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="55"/>
        <source>Remove jobs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="94"/>
        <source>Process jobs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="97"/>
        <source>Run</source>
        <translation type="unfinished">Lancer</translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="120"/>
        <source>Abort processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="123"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="162"/>
        <source>Reset status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="165"/>
        <source>Reset</source>
        <translation type="unfinished">Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="204"/>
        <source>Save job list to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="207"/>
        <source>Save...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="230"/>
        <source>Open job list from files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="233"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="269"/>
        <source>Show processing log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="272"/>
        <source>Show log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="314"/>
        <source>Close batch manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.ui" line="317"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="196"/>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="211"/>
        <source>Job list files (*.ptb);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="199"/>
        <source>Save job list file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptBatchWindow.cpp" line="214"/>
        <source>Open job list files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptColorSelectButton</name>
    <message>
        <location filename="../Sources/ptColorSelectButton.cpp" line="93"/>
        <source>
currently: red %1, green %2, blue %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptCurveWindow</name>
    <message>
        <source>A&amp;daptive</source>
        <translation type="obsolete">Adaptatif</translation>
    </message>
    <message>
        <source>Adaptive saturation</source>
        <translation type="obsolete">Saturation adaptative</translation>
    </message>
    <message>
        <source>A&amp;bsolute</source>
        <translation type="obsolete">Absolu</translation>
    </message>
    <message>
        <source>Absolute saturation</source>
        <translation type="obsolete">Saturation absolue</translation>
    </message>
    <message>
        <source>By l&amp;uminance</source>
        <translation type="obsolete">Par luminance</translation>
    </message>
    <message>
        <source>Mask by luminance</source>
        <translation type="obsolete">Masque par luminance</translation>
    </message>
    <message>
        <source>By c&amp;olor</source>
        <translation type="obsolete">Par couleur</translation>
    </message>
    <message>
        <source>Mask by color</source>
        <translation type="obsolete">Masque par couleur</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="650"/>
        <source>L&amp;uminance mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="654"/>
        <source>C&amp;olor mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="663"/>
        <source>&amp;Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="664"/>
        <source>Linear interpolation</source>
        <translation>Interpolation linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="668"/>
        <source>&amp;Spline</source>
        <translation>Spline</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="669"/>
        <source>Spline interpolation</source>
        <translation>Interpolation par spline</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="673"/>
        <source>&amp;Cosine</source>
        <translation>Cosinus</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="674"/>
        <source>Cosine interpolation</source>
        <translation>Interpolation par cosinus</translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="683"/>
        <source>Open &amp;file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptCurveWindow.cpp" line="684"/>
        <source>Open anchor curve file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFileMgrWindow</name>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.ui" line="119"/>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="114"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.ui" line="172"/>
        <source>Open bookmark list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="82"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="84"/>
        <source>Directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="91"/>
        <source>Bookmark current folder (Ctrl+B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="93"/>
        <source>Bookmark current directory (Ctrl+B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="660"/>
        <source>&amp;Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="660"/>
        <source>Alt+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="664"/>
        <source>&amp;Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="664"/>
        <source>Alt+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="668"/>
        <source>&amp;Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="668"/>
        <source>Alt+3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="673"/>
        <source>Show &amp;folder thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="675"/>
        <source>Show &amp;directory thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="687"/>
        <source>Show &amp;image preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="687"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="691"/>
        <source>Show &amp;sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="691"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="695"/>
        <source>&amp;Save thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="698"/>
        <source>&amp;Close file manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="698"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="701"/>
        <source>Show RAWs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="705"/>
        <source>Show bitmaps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptFileMgrWindow.cpp" line="715"/>
        <source>Thumbnail &amp;view</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_ABCurves</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ABCurves.cpp" line="45"/>
        <source>a* b* curves</source>
        <translation>Courbes a* b*</translation>
    </message>
</context>
<context>
    <name>ptFilter_BlackWhite</name>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="53"/>
        <source>Black and White</source>
        <translation type="unfinished">Noir et blanc</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="61"/>
        <source>Low sensitivity</source>
        <translation type="unfinished">Faible sensibilité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="62"/>
        <source>High sensitivity</source>
        <translation type="unfinished">Haute sensibilité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="63"/>
        <source>Hyperpanchromatic</source>
        <translation type="unfinished">Hyper-panchromatique</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="64"/>
        <source>Orthochromatic</source>
        <translation type="unfinished">Orthochromatique</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="65"/>
        <source>Normal contrast</source>
        <translation type="unfinished">Contraste normal</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="66"/>
        <source>High contrast</source>
        <translation type="unfinished">Fort contraste</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="67"/>
        <source>Luminance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="68"/>
        <source>Landscape</source>
        <translation type="unfinished">Paysage</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="69"/>
        <source>Face in interior</source>
        <translation type="unfinished">Visage en intérieur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="70"/>
        <source>Channel mixer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="74"/>
        <source>None</source>
        <translation type="unfinished">Aucun</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="75"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="76"/>
        <source>Orange</source>
        <translation type="unfinished">Orange</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="77"/>
        <source>Yellow</source>
        <translation type="unfinished">Jaune</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="78"/>
        <source>Lime</source>
        <translation type="unfinished">Lime</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="79"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="80"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="81"/>
        <source>Fake IR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="86"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="87"/>
        <source>Film type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="87"/>
        <source>type of analog film to emulate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="88"/>
        <source>Color filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="88"/>
        <source>type of color filter to emulate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="89"/>
        <source>Red multiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="90"/>
        <source>Green multiplier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_BlackWhite.cpp" line="91"/>
        <source>Blue multiplier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Brightness</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="48"/>
        <source>Brightness</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="57"/>
        <source>Catch white</source>
        <translation>Capturer le blanc</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="57"/>
        <source>Darken the bright parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="58"/>
        <source>Catch black</source>
        <translation>Capturer le noir</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="58"/>
        <source>Brighten the dark parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="59"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Brightness.cpp" line="59"/>
        <source>Exposure gain</source>
        <translation>Gain d&apos;exposition</translation>
    </message>
</context>
<context>
    <name>ptFilter_ChannelMixer</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="70"/>
        <source>Channel mixer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="79"/>
        <source>contribution of Red input to Red output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="80"/>
        <source>contribution of Red input to Green output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="81"/>
        <source>contribution of Red input to Blue output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="82"/>
        <source>contribution of Green input to Red output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="83"/>
        <source>contribution of Green input to Green output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="84"/>
        <source>contribution of Green input to Blue output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="85"/>
        <source>contribution of Blue input to Red output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="86"/>
        <source>contribution of Blue input to Green output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="87"/>
        <source>contribution of Blue input to Blue output </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="173"/>
        <source>Load channel mixer configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="184"/>
        <source>Error loading channel mixer configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="197"/>
        <source>Save channel mixer configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="208"/>
        <source>Save Channelmixer</source>
        <translation type="unfinished">Enregistrer le mixeur de canaux</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="209"/>
        <source>Give a description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="221"/>
        <source>Error saving channel mixer configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="235"/>
        <source>Could not open %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="264"/>
        <source>%1 has wrong format at line %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="272"/>
        <source>Error reading %1 at line %2 (out of range: %3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ChannelMixer.cpp" line="303"/>
        <source>Could not create %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorBoost</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorBoost.cpp" line="48"/>
        <source>Color boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorBoost.cpp" line="57"/>
        <source>Strength in a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorBoost.cpp" line="58"/>
        <source>Strength in b</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorContrast</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorContrast.cpp" line="49"/>
        <source>Color contrast</source>
        <translation>Contraste de couleur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorContrast.cpp" line="58"/>
        <source>Opacity</source>
        <translation>Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorContrast.cpp" line="59"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorContrast.cpp" line="60"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorContrast.cpp" line="61"/>
        <source>Halo control</source>
        <translation>Contrôle du halo</translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorDenoise</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorDenoise.cpp" line="54"/>
        <source>Color denoising</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorDenoise.cpp" line="63"/>
        <source>A channel strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorDenoise.cpp" line="64"/>
        <source>A channel scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorDenoise.cpp" line="65"/>
        <source>B channel strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorDenoise.cpp" line="66"/>
        <source>B channel scale</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorEnhancement</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorEnhancement.cpp" line="48"/>
        <source>Color enhancement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorEnhancement.cpp" line="57"/>
        <source>Enhance shadows</source>
        <translation>Améliorer les ombres</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorEnhancement.cpp" line="58"/>
        <source>Enhance highlights</source>
        <translation>Améliorer les hautes lumières</translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorIntensity</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorIntensity.cpp" line="51"/>
        <source>Color Intensity</source>
        <translation>Intensité de couleur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorIntensity.cpp" line="60"/>
        <source>Vibrance</source>
        <translation>Vibrance</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorIntensity.cpp" line="61"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorIntensity.cpp" line="62"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorIntensity.cpp" line="63"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
</context>
<context>
    <name>ptFilter_ColorTone</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="61"/>
        <source>Color toning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="87"/>
        <source>Mask type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="88"/>
        <source>toning color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="89"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="90"/>
        <source>Lower limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="91"/>
        <source>Upper limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ColorTone.cpp" line="92"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_CrossProcessing</name>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="50"/>
        <source>Cross processing</source>
        <translation type="unfinished">Traitement croisé</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="58"/>
        <source>Disabled</source>
        <translation type="unfinished">Désactivé</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="59"/>
        <source>Green - yellow</source>
        <translation type="unfinished">Vert - jaune</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="60"/>
        <source>Green - cyan</source>
        <translation type="unfinished">Vert - cyan</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="61"/>
        <source>Red - yellow</source>
        <translation type="unfinished">Rouge - jaune</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="62"/>
        <source>Red - magenta</source>
        <translation type="unfinished">Rouge - mangenta</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="63"/>
        <source>Blue - cyan</source>
        <translation type="unfinished">Bleu - cyan</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="64"/>
        <source>Blue - magenta</source>
        <translation type="unfinished">Bleu - mangenta</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="69"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="70"/>
        <source>Main color</source>
        <translation type="unfinished">Couleur principale</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="70"/>
        <source>intensity of the main color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="71"/>
        <source>Secondary color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_CrossProcessing.cpp" line="71"/>
        <source>intensity of the secondary color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Defringe</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="59"/>
        <source>Defringe</source>
        <translation type="unfinished">Détourage</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="69"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="70"/>
        <source>Threshold</source>
        <translation type="unfinished">Seuil</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="71"/>
        <source>Masks tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="71"/>
        <source>fine tunes the color masks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="72"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="73"/>
        <source>Yellow</source>
        <translation type="unfinished">Jaune</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="74"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="75"/>
        <source>Cyan</source>
        <translation type="unfinished">Cyan</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="76"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Defringe.cpp" line="77"/>
        <source>Purple</source>
        <translation type="unfinished">Violet</translation>
    </message>
</context>
<context>
    <name>ptFilter_DetailCurve</name>
    <message>
        <location filename="../Sources/filters/ptFilter_DetailCurve.cpp" line="50"/>
        <source>Detail curve</source>
        <translation>Courbe de détail</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_DetailCurve.cpp" line="67"/>
        <source>Halo control</source>
        <translation>Contrôle du halo</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_DetailCurve.cpp" line="68"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_DetailCurve.cpp" line="69"/>
        <source>Anti badpixel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Drc</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Drc.cpp" line="50"/>
        <source>Dynamic range compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Drc.cpp" line="59"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Drc.cpp" line="60"/>
        <source>Bias</source>
        <translation>Biais</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Drc.cpp" line="61"/>
        <source>Color adaption</source>
        <translation>Adaptation de couleur</translation>
    </message>
</context>
<context>
    <name>ptFilter_EAWavelets</name>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="59"/>
        <source>Edge avoiding wavelets</source>
        <translation type="unfinished">Ondelettes à évitement de bords</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="68"/>
        <source>Master</source>
        <translation type="unfinished">Maître</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="68"/>
        <source>Quick setup of all levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="69"/>
        <source>Level 1</source>
        <translation type="unfinished">Niveau 1</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="70"/>
        <source>Level 2</source>
        <translation type="unfinished">Niveau 2</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="71"/>
        <source>Level 3</source>
        <translation type="unfinished">Niveau 3</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="72"/>
        <source>Level 4</source>
        <translation type="unfinished">Niveau 4</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="73"/>
        <source>Level 5</source>
        <translation type="unfinished">Niveau 5</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_EAWavelets.cpp" line="74"/>
        <source>Level 6</source>
        <translation type="unfinished">Niveau 6</translation>
    </message>
</context>
<context>
    <name>ptFilter_Exposure</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="58"/>
        <source>Exposure</source>
        <translation type="unfinished">Exposition</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="66"/>
        <source>Manual</source>
        <translation type="unfinished">Manuel</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="67"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="68"/>
        <source>Like UFRaw</source>
        <translation type="unfinished">Comme UFraw</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="72"/>
        <source>Hard</source>
        <translation type="unfinished">Dur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="73"/>
        <source>Ratio</source>
        <translation type="unfinished">Rapport</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="74"/>
        <source>Film curve</source>
        <translation type="unfinished">Courbe de film</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="79"/>
        <source>Exposure mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="80"/>
        <source>Clipping mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="81"/>
        <source>Exposure correction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="81"/>
        <source>Exposure correction in f-stops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="82"/>
        <source>Target white percentage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="82"/>
        <source>Percentage of the image that should become white</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="83"/>
        <source>White level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Exposure.cpp" line="83"/>
        <source>Brightness of the “white”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_FilmGrain</name>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="70"/>
        <source>Film grain simulation</source>
        <translation type="unfinished">Simulation de grain de film</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="78"/>
        <source>Soft gaussian</source>
        <translation type="unfinished">Gaussien doux</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="79"/>
        <source>Soft uniform</source>
        <translation type="unfinished">Uniforme doux</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="80"/>
        <source>Soft salt &amp; pepper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="81"/>
        <source>Hard gaussian</source>
        <translation type="unfinished">Gaussien dur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="82"/>
        <source>Hard uniform</source>
        <translation type="unfinished">Uniforme dur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="83"/>
        <source>Hard salt &amp; pepper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="88"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="96"/>
        <source>Mask type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="89"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="97"/>
        <source>Noise type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="90"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="98"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="91"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="99"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="92"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="100"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="93"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="101"/>
        <source>Lower limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="94"/>
        <location filename="../Sources/filters/ptFilter_FilmGrain.cpp" line="102"/>
        <source>Upper limit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_GammaTool</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GammaTool.cpp" line="47"/>
        <source>Gamma adjustment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GammaTool.cpp" line="56"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GammaTool.cpp" line="57"/>
        <source>Linearity</source>
        <translation>Linéarité</translation>
    </message>
</context>
<context>
    <name>ptFilter_GradientSharpen</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="52"/>
        <source>Gradien sharpen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="61"/>
        <source>Passes</source>
        <translation type="unfinished">Passes</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="62"/>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="63"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="64"/>
        <source>Halo control</source>
        <translation type="unfinished">Contrôle du halo</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="65"/>
        <source>Weight</source>
        <translation type="unfinished">Poids</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradientSharpen.cpp" line="66"/>
        <source>Hotpixel reduction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_GradualBlur</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="63"/>
        <source>Gradual blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="71"/>
        <source>Linear</source>
        <translation type="unfinished">Linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="72"/>
        <source>Vignette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="73"/>
        <source>Linear mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="74"/>
        <source>Vignette mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="79"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="81"/>
        <source>Shape</source>
        <translation type="unfinished">Échelle</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="81"/>
        <source>shape of the vignette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="82"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="83"/>
        <source>Lower level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="84"/>
        <source>Upper level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="85"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="86"/>
        <source>Angle</source>
        <translation type="unfinished">Angle</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="87"/>
        <source>Roundness</source>
        <translation type="unfinished">Rondeur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="88"/>
        <source>Horizontal center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualBlur.cpp" line="89"/>
        <source>vertical center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_GradualOverlay</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="62"/>
        <source>Gradual overlay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="82"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="82"/>
        <source>overlay mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="83"/>
        <source>overlay color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="84"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="85"/>
        <source>Angle</source>
        <translation type="unfinished">Angle</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="86"/>
        <source>Lower level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="87"/>
        <source>Upper level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GradualOverlay.cpp" line="88"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_GreyCStoration</name>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="67"/>
        <source>GreyCStoration on L</source>
        <translation type="unfinished">GreyCStoration sur L</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="75"/>
        <source>All Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="76"/>
        <source>Shadows 1</source>
        <translation type="unfinished">Ombres 1</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="77"/>
        <source>Shadows 2</source>
        <translation type="unfinished">Ombres 2</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="78"/>
        <source>Shadows 3</source>
        <translation type="unfinished">Ombres 3</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="79"/>
        <source>Shadows 4</source>
        <translation type="unfinished">Ombres 4</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="80"/>
        <source>Shadows 5</source>
        <translation type="unfinished">Ombres 5</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="84"/>
        <source>Nearest neighbour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="85"/>
        <source>Linear</source>
        <translation type="unfinished">Linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="86"/>
        <source>Runge-Kutta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="93"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="94"/>
        <source>Denoise mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="95"/>
        <source>Fast Gaussian approximation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="96"/>
        <source>Iterations</source>
        <translation type="unfinished">Itérations</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="97"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="98"/>
        <source>Amplitude</source>
        <translation type="unfinished">Amplitude</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="99"/>
        <source>Sharpness</source>
        <translation type="unfinished">Dureté</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="100"/>
        <source>Anisotropy</source>
        <translation type="unfinished">Anisotropie</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="101"/>
        <source>Gradient smoothness</source>
        <translation type="unfinished">Douceur du gradient</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="101"/>
        <source>alpha standard deviation of the gradient blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="102"/>
        <source>Tensor smoothness</source>
        <translation type="unfinished">Douceur du tenseur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="102"/>
        <source>sigma standard deviation of the gradient blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="103"/>
        <source>Spacial precision</source>
        <translation type="unfinished">Précision spatiale</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="103"/>
        <source>dl spatial discretization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="104"/>
        <source>Angular precision</source>
        <translation type="unfinished">Précision angulaire</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="104"/>
        <source>da angular discretization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="105"/>
        <source>Value precision</source>
        <translation type="unfinished">Précision de la valeur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="105"/>
        <source>precision of the diffusion process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_GreyCStoration.cpp" line="106"/>
        <source>Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Highlights</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="48"/>
        <source>Highlights</source>
        <translation>Hautes lumières</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="57"/>
        <source>Reg highlights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="57"/>
        <source>Adjusts brightness of highlights in R channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="58"/>
        <source>Green highlights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="58"/>
        <source>Adjusts brightness of highlights in G channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="59"/>
        <source>Blue highlights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Highlights.cpp" line="59"/>
        <source>Adjusts brightness of highlights in B channel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_HighpassSharpen</name>
    <message>
        <location filename="../Sources/filters/ptFilter_HighpassSharpen.cpp" line="57"/>
        <source>Highpass sharpen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_HighpassSharpen.cpp" line="66"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_HighpassSharpen.cpp" line="67"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_HighpassSharpen.cpp" line="68"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_HighpassSharpen.cpp" line="69"/>
        <source>Denoising strength</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_ImpulseNR</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ImpulseNR.cpp" line="47"/>
        <source>Impulse noise reduction</source>
        <translation type="unfinished">Réduction du bruit d&apos;impulsion</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ImpulseNR.cpp" line="56"/>
        <source>Lightness threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ImpulseNR.cpp" line="57"/>
        <source>Color threshold</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_InvDiffSharpen</name>
    <message>
        <location filename="../Sources/filters/ptFilter_InvDiffSharpen.cpp" line="50"/>
        <source>Inverse diffusion sharpen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_InvDiffSharpen.cpp" line="60"/>
        <source>Iterations</source>
        <translation type="unfinished">Itérations</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_InvDiffSharpen.cpp" line="61"/>
        <source>Only edges</source>
        <translation type="unfinished">Bords seuls</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_InvDiffSharpen.cpp" line="62"/>
        <source>Amplitude</source>
        <translation type="unfinished">Amplitude</translation>
    </message>
</context>
<context>
    <name>ptFilter_LMHRecovery</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="63"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="71"/>
        <source>Low/mid/highlight recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">Désactivé</translation>
    </message>
    <message>
        <source>Shadows</source>
        <translation type="vanished">Ombres</translation>
    </message>
    <message>
        <source>Midtones</source>
        <translation type="vanished">Tons moyens</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="vanished">Hautes lumières</translation>
    </message>
    <message>
        <source>All values</source>
        <translation type="vanished">Toutes les valeurs</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="79"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="84"/>
        <source>Mask type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="80"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="85"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="81"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="86"/>
        <source>Lower limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="82"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="87"/>
        <source>Upper limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="83"/>
        <location filename="../Sources/filters/ptFilter_LMHRecovery.cpp" line="88"/>
        <source>Softness</source>
        <translation>Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_LabTransform</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="46"/>
        <source>Lab transform</source>
        <translation>Transformation Lab</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="54"/>
        <source>Regular L*</source>
        <translation>L* classique</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="55"/>
        <source>R -&gt; L*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="56"/>
        <source>G -&gt; L*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="57"/>
        <source>B -&gt; L*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LabTransform.cpp" line="61"/>
        <source>Transformation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Levels</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Levels.cpp" line="51"/>
        <location filename="../Sources/filters/ptFilter_Levels.cpp" line="59"/>
        <source>Levels</source>
        <translation>Niveaux</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Levels.cpp" line="68"/>
        <source>Blackpoint</source>
        <translation>Point noir</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Levels.cpp" line="69"/>
        <source>Whitepoint</source>
        <translation>Point blanc</translation>
    </message>
</context>
<context>
    <name>ptFilter_LocalContrast</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="65"/>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="74"/>
        <source>Local contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Désactivé</translation>
    </message>
    <message>
        <source>Shadows</source>
        <translation type="obsolete">Ombres</translation>
    </message>
    <message>
        <source>Midtones</source>
        <translation type="obsolete">Tons moyens</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="obsolete">Hautes lumières</translation>
    </message>
    <message>
        <source>All values</source>
        <translation type="obsolete">Toutes les valeurs</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="114"/>
        <source>Mask type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="115"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="116"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="117"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="118"/>
        <source>Halo control</source>
        <translation type="unfinished">Contrôle du halo</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="119"/>
        <source>Lower limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="120"/>
        <source>Upper limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrast.cpp" line="121"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_LocalContrastStretch</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrastStretch.cpp" line="58"/>
        <source>Local contrast stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrastStretch.cpp" line="84"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrastStretch.cpp" line="85"/>
        <source>Feather</source>
        <translation type="unfinished">Plume</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrastStretch.cpp" line="86"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LocalContrastStretch.cpp" line="87"/>
        <source>Masking</source>
        <translation type="unfinished">Masquage</translation>
    </message>
</context>
<context>
    <name>ptFilter_LumaDenoise</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoise.cpp" line="54"/>
        <source>Luminance denoising</source>
        <translation type="unfinished">Débruitage de la luminance</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoise.cpp" line="63"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoise.cpp" line="64"/>
        <source>Edge Threshold</source>
        <translation type="unfinished">Seuil des bords</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoise.cpp" line="65"/>
        <source>L scale</source>
        <translation type="unfinished">Échelle pour L</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoise.cpp" line="66"/>
        <source>L strength</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_LumaDenoiseCurve</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoiseCurve.cpp" line="54"/>
        <source>Luminance denoise curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoiseCurve.cpp" line="68"/>
        <source>L strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaDenoiseCurve.cpp" line="69"/>
        <source>L scale</source>
        <translation>Échelle pour L</translation>
    </message>
</context>
<context>
    <name>ptFilter_LumaSatAdjust</name>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="56"/>
        <source>Luminance adjustment</source>
        <translation>Ajustement de la luminance</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="66"/>
        <source>Saturation adjustment</source>
        <translation>Ajustement de la saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="75"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="76"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="77"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="78"/>
        <source>Light green</source>
        <translation>Vert clair</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="79"/>
        <source>Dark green</source>
        <translation>Vert sombre</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="80"/>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="81"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_LumaSatAdjust.cpp" line="82"/>
        <source>Magenta</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Normalization</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Normalization.cpp" line="47"/>
        <source>Normalization</source>
        <translation>Normalisation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Normalization.cpp" line="56"/>
        <source>Opacity</source>
        <translation>Opacité</translation>
    </message>
</context>
<context>
    <name>ptFilter_Outline</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="54"/>
        <source>Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="62"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="63"/>
        <source>SoftLight</source>
        <translation>Lumière douce</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="64"/>
        <source>Multiply</source>
        <translation>Multiplier</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="65"/>
        <source>Screen</source>
        <translation>Écran</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="66"/>
        <source>Gamma dark</source>
        <translation>Gamma sombre</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="67"/>
        <source>Gamma bright</source>
        <translation>Gamma clair</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="68"/>
        <source>Color burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="69"/>
        <source>Color dodge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="70"/>
        <source>Darken only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="71"/>
        <source>Lighten only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="72"/>
        <source>Show outlines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="85"/>
        <source>Overlay mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="86"/>
        <source>Image on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="86"/>
        <source>Overlay the image on top of the outlines instead of vice versa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="75"/>
        <source>Backward finite differences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="76"/>
        <source>Centered finite differences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="77"/>
        <source>Forward finite differences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="78"/>
        <source>Sobel masks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="79"/>
        <source>Rotation invariant masks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="80"/>
        <source>Deriche recursive filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="87"/>
        <source>Outlines mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="87"/>
        <source>Method for calculating the outline gradients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="88"/>
        <source>Color weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="88"/>
        <source>Weight of the A/B channels in the outlines calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Outline.cpp" line="89"/>
        <source>Blur radius</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_PyramidDenoise</name>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="55"/>
        <source>Pyramid denoising</source>
        <translation type="unfinished">Débruitage pyramidal</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="64"/>
        <source>Lightness strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="64"/>
        <source>affects L channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="65"/>
        <source>Color strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="65"/>
        <source>affects a and b channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="66"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_PyramidDenoise.cpp" line="67"/>
        <source>Levels</source>
        <translation type="unfinished">Niveaux</translation>
    </message>
</context>
<context>
    <name>ptFilter_ReinhardBrighten</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ReinhardBrighten.cpp" line="50"/>
        <source>Reinhard brighten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ReinhardBrighten.cpp" line="60"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ReinhardBrighten.cpp" line="61"/>
        <source>Brightness</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ReinhardBrighten.cpp" line="62"/>
        <source>Chrominance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ReinhardBrighten.cpp" line="63"/>
        <source>Lightness tweak</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_SatCurve</name>
    <message>
        <location filename="../Sources/filters/ptFilter_SatCurve.cpp" line="49"/>
        <source>Saturation curve</source>
        <translation>Courbe de saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SatCurve.cpp" line="57"/>
        <source>Absolute</source>
        <translation>Absolu</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SatCurve.cpp" line="58"/>
        <source>Adaptive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SatCurve.cpp" line="67"/>
        <source>Saturation mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Saturation</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Saturation.cpp" line="46"/>
        <source>Saturation adjustment</source>
        <translation>Ajustement de la saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Saturation.cpp" line="55"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
</context>
<context>
    <name>ptFilter_ShadowsHighlights</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ShadowsHighlights.cpp" line="50"/>
        <source>Shadows/Highlights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ShadowsHighlights.cpp" line="60"/>
        <source>Fine detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ShadowsHighlights.cpp" line="61"/>
        <source>Coarse detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ShadowsHighlights.cpp" line="62"/>
        <source>Scale</source>
        <translation>Redimensionnement</translation>
    </message>
</context>
<context>
    <name>ptFilter_SigContrast</name>
    <message>
        <location filename="../Sources/filters/ptFilter_SigContrast.cpp" line="61"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SigContrast.cpp" line="62"/>
        <source>Threshold</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SigContrast.cpp" line="98"/>
        <source>Lightness contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SigContrast.cpp" line="108"/>
        <source>Sigmoidal contrast</source>
        <translation>Contraste en S</translation>
    </message>
</context>
<context>
    <name>ptFilter_SimpleTone</name>
    <message>
        <location filename="../Sources/filters/ptFilter_SimpleTone.cpp" line="49"/>
        <source>Simple toning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SimpleTone.cpp" line="58"/>
        <source>Red</source>
        <translation type="unfinished">Rouge</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SimpleTone.cpp" line="59"/>
        <source>Green</source>
        <translation type="unfinished">Vert</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SimpleTone.cpp" line="60"/>
        <source>Blue</source>
        <translation type="unfinished">Bleu</translation>
    </message>
</context>
<context>
    <name>ptFilter_SoftglowOrton</name>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="56"/>
        <source>Softglow/Orton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="64"/>
        <source>Disabled</source>
        <translation type="unfinished">Désactivé</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="65"/>
        <source>Lighten</source>
        <translation type="unfinished">Éclaircir</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="66"/>
        <source>Screen</source>
        <translation type="unfinished">Écran</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="67"/>
        <source>Softlight</source>
        <translation type="unfinished">Lumière douce</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="68"/>
        <source>Normal</source>
        <translation type="unfinished">Normal</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="69"/>
        <source>Orton screen</source>
        <translation type="unfinished">Effet Orton</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="70"/>
        <source>Orton softlight</source>
        <translation type="unfinished">Lumière douce d&apos;Orton</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="75"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="76"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="77"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="78"/>
        <source>Contrast</source>
        <translation type="unfinished">Contraste</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_SoftglowOrton.cpp" line="79"/>
        <source>Saturation</source>
        <translation type="unfinished">Saturation</translation>
    </message>
</context>
<context>
    <name>ptFilter_SpotTuning</name>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="68"/>
        <source>Spot tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="84"/>
        <source>Use maximum radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="85"/>
        <source>Maximum radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="85"/>
        <source>Pixels outside this radius will never be included in the mask.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="86"/>
        <source>Brightness/color ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="86"/>
        <source>Defines how brightness and color affect the threshold.
0.0: ignore color, 1.0: ignore brightness, 0.5: equal weight for both</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="87"/>
        <source>Threshold</source>
        <translation type="unfinished">Seuil</translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="87"/>
        <source>Maximum amount a pixel may differ from the spot&apos;s source pixel to get included in the mask.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="88"/>
        <source>Saturation</source>
        <translation type="unfinished">Saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="89"/>
        <source>Adaptive saturation</source>
        <translation type="unfinished">Saturation adaptative</translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="89"/>
        <source>Prevent clipping when adjusting saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="90"/>
        <source>Color shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptFilter_SpotTuning.cpp" line="129"/>
        <source>Luminance curve</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_StdCurve</name>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="126"/>
        <source>RGB curve</source>
        <translation>Courbe RVB</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="139"/>
        <source>Texture curve</source>
        <translation>Courbe de texture</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="152"/>
        <source>Luminance by hue curve</source>
        <translation>Luminance par courbe de teinte</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="165"/>
        <source>Hue curve</source>
        <translation>Courbe de teinte</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="178"/>
        <source>L* curve</source>
        <translation>Courbe L*</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="191"/>
        <source>R tone curve</source>
        <translation>Courbe de ton R</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="204"/>
        <source>G tone curve</source>
        <translation>Courbe de ton V</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="217"/>
        <source>B tone curve</source>
        <translation>Courbe de ton B</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_StdCurve.cpp" line="230"/>
        <source>After gamma curve</source>
        <translation>Courbe post-gamma</translation>
    </message>
</context>
<context>
    <name>ptFilter_TextureContrast</name>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="60"/>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="69"/>
        <source>Texture contrast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="82"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="83"/>
        <source>Scale</source>
        <translation type="unfinished">Redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="84"/>
        <source>Threshold</source>
        <translation type="unfinished">Seuil</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="85"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="86"/>
        <source>Denoise</source>
        <translation type="unfinished">Débruitage</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureContrast.cpp" line="87"/>
        <source>Masking</source>
        <translation type="unfinished">Masquage</translation>
    </message>
</context>
<context>
    <name>ptFilter_TextureOverlay</name>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="65"/>
        <source>Texture overlay</source>
        <translation type="unfinished">Calque de texture</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="80"/>
        <source>Overlay mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="81"/>
        <source>Mask mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="83"/>
        <source>Opacity</source>
        <translation type="unfinished">Opacité</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="84"/>
        <source>Saturation</source>
        <translation type="unfinished">Saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="85"/>
        <source>Shape</source>
        <translation type="unfinished">Échelle</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="85"/>
        <source>Shape of the vignette</source>
        <translation type="unfinished">Échelle de la vignette</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="86"/>
        <source>Inner radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="87"/>
        <source>Outer radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="88"/>
        <source>Roundness</source>
        <translation type="unfinished">Rondeur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="89"/>
        <source>Horizontal center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="90"/>
        <source>Vertical center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="91"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_TextureOverlay.cpp" line="259"/>
        <source>Load overlay image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Tone</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="62"/>
        <source>Tone</source>
        <translation>Tons</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="71"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="74"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="77"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="80"/>
        <source>Saturation</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="72"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="75"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="78"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="81"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="73"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="76"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="79"/>
        <location filename="../Sources/filters/ptFilter_Tone.cpp" line="82"/>
        <source>Hue</source>
        <translation>Teinte</translation>
    </message>
</context>
<context>
    <name>ptFilter_ToneAdjust</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="55"/>
        <source>Tone adjustment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="67"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <source>Shadows</source>
        <translation type="vanished">Ombres</translation>
    </message>
    <message>
        <source>Midtones</source>
        <translation type="vanished">Tons moyens</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="vanished">Hautes lumières</translation>
    </message>
    <message>
        <source>All values</source>
        <translation type="vanished">Toutes les valeurs</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="68"/>
        <source>Mask mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="69"/>
        <source>Saturation</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="70"/>
        <source>Hue</source>
        <translation>Teinte</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="71"/>
        <source>Lower limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="72"/>
        <source>Upper limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ToneAdjust.cpp" line="73"/>
        <source>Softness</source>
        <translation>Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_UnsharpMask</name>
    <message>
        <location filename="../Sources/filters/ptFilter_UnsharpMask.cpp" line="55"/>
        <source>Unsharp mask (USM)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_UnsharpMask.cpp" line="65"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_UnsharpMask.cpp" line="66"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_UnsharpMask.cpp" line="67"/>
        <source>Radius</source>
        <translation type="unfinished">Rayon</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_UnsharpMask.cpp" line="68"/>
        <source>Threshold</source>
        <translation type="unfinished">Seuil</translation>
    </message>
</context>
<context>
    <name>ptFilter_ViewLab</name>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="47"/>
        <source>View Lab channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="55"/>
        <source>Lab (normal image)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="56"/>
        <source>L channel only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="57"/>
        <source>Structure in L channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="58"/>
        <source>a channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="59"/>
        <source>b channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="60"/>
        <source>C channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="61"/>
        <source>H channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_ViewLab.cpp" line="66"/>
        <source>Channel mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptFilter_Vignette</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="61"/>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="70"/>
        <source>Vignette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="81"/>
        <source>Disabled</source>
        <translation type="unfinished">Désactivé</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="82"/>
        <source>Soft</source>
        <translation type="unfinished">Doux</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="83"/>
        <source>Hard</source>
        <translation type="unfinished">Dur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="84"/>
        <source>Fancy</source>
        <translation type="unfinished">Décoratif</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="85"/>
        <source>Show mask</source>
        <translation type="unfinished">Afficher le masque</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="89"/>
        <source>Mask type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="90"/>
        <source>Shape</source>
        <translation type="unfinished">Échelle</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="90"/>
        <source>Shape of the vignette</source>
        <translation type="unfinished">Échelle de la vignette</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="91"/>
        <source>Strength</source>
        <translation type="unfinished">Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="92"/>
        <source>Inner radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="93"/>
        <source>Outer radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="94"/>
        <source>Roundness</source>
        <translation type="unfinished">Rondeur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="95"/>
        <source>Horizontal center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="96"/>
        <source>Vertical center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Vignette.cpp" line="97"/>
        <source>Softness</source>
        <translation type="unfinished">Douceur</translation>
    </message>
</context>
<context>
    <name>ptFilter_WaveletDenoise</name>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="62"/>
        <source>Wavelet denoising</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="71"/>
        <source>L strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="72"/>
        <source>L softness</source>
        <translation type="unfinished">Douceur pour L</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="73"/>
        <source>Sharpness</source>
        <translation type="unfinished">Dureté</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="74"/>
        <source>Anisotropy</source>
        <translation type="unfinished">Anisotropie</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="75"/>
        <source>Gradient smoothness</source>
        <translation type="unfinished">Douceur du gradient</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="76"/>
        <source>Tensor smoothness</source>
        <translation type="unfinished">Douceur du tenseur</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="77"/>
        <source>A strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="78"/>
        <source>A softness</source>
        <translation type="unfinished">Douceur sur A</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="79"/>
        <source>B strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_WaveletDenoise.cpp" line="80"/>
        <source>B softness</source>
        <translation type="unfinished">Douceur sur B</translation>
    </message>
</context>
<context>
    <name>ptFilter_Wiener</name>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="54"/>
        <source>Wiener sharpen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="63"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="63"/>
        <source>Switch filter on and off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="64"/>
        <source>Only edges</source>
        <translation>Bords seuls</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="64"/>
        <source>Sharpen only edges</source>
        <translation>Accentuer seulement les bords</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="65"/>
        <source>Strength</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="66"/>
        <source>Gaussian</source>
        <translation>Gaussien</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="67"/>
        <source>Box</source>
        <translation>Cadre</translation>
    </message>
    <message>
        <location filename="../Sources/filters/ptFilter_Wiener.cpp" line="68"/>
        <source>Lens blur</source>
        <translation>Flou d&apos;objectif</translation>
    </message>
</context>
<context>
    <name>ptGroupBox</name>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="79"/>
        <source>Complex filter. Might be slow.</source>
        <translation>Filtre complexe. Peut être lent.</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="93"/>
        <source>Open help page in web browser.</source>
        <translation>Ouvrir la page d&apos;aide dans le navigateur web.</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="131"/>
        <source>&amp;Hide</source>
        <translation>Masquer</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="484"/>
        <source>Bl&amp;ock</source>
        <translation>Bloquer</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="141"/>
        <source>&amp;Reset</source>
        <translation>Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="146"/>
        <source>&amp;Save preset</source>
        <translation>Enregistrer un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="151"/>
        <source>&amp;Append preset</source>
        <translation>Ajouter un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="332"/>
        <source>Settings File</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="481"/>
        <source>All&amp;ow</source>
        <translation>Autoriser</translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="499"/>
        <location filename="../Sources/ptGroupBox.cpp" line="521"/>
        <location filename="../Sources/ptGroupBox.cpp" line="536"/>
        <source>Add to &amp;favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptGroupBox.cpp" line="502"/>
        <location filename="../Sources/ptGroupBox.cpp" line="524"/>
        <location filename="../Sources/ptGroupBox.cpp" line="539"/>
        <source>Remove from &amp;favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favourite tool</source>
        <translation type="obsolete">Outil favori</translation>
    </message>
    <message>
        <source>Normal tool</source>
        <translation type="obsolete">Outil normal</translation>
    </message>
</context>
<context>
    <name>ptHistogramWindow</name>
    <message>
        <source>X axis logarithmic</source>
        <translation type="obsolete">Axe X logarithmique</translation>
    </message>
    <message>
        <source>Y axis logarithmic</source>
        <translation type="obsolete">Axe Y logarithmique</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="92"/>
        <source>Logarithmic &amp;X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="97"/>
        <source>Logarithmic &amp;Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="102"/>
        <source>&amp;Selection</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="103"/>
        <source>Histogram only on a part of the image</source>
        <translation>Histogramme seulement sur une partie de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="111"/>
        <source>&amp;Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="112"/>
        <source>Use data from linear pipe</source>
        <translation>Utiliser les donner du tube linéaire</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="116"/>
        <source>&amp;Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="117"/>
        <source>Use data with preview profile</source>
        <translation>Utiliser les données avec le profil d&apos;aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="121"/>
        <source>&amp;Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="122"/>
        <source>Use data with output profile</source>
        <translation>Utiliser les données avec le profil de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="138"/>
        <location filename="../Sources/ptHistogramWindow.cpp" line="139"/>
        <source>RGB</source>
        <translation>RVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="143"/>
        <source>&amp;R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="144"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="148"/>
        <source>&amp;G</source>
        <translation>&amp;V</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="149"/>
        <source>G</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="153"/>
        <source>&amp;B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="154"/>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="493"/>
        <source>RAW thumbnail is used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="559"/>
        <source>Display &amp;channels</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="../Sources/ptHistogramWindow.cpp" line="566"/>
        <source>Display &amp;mode</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ptImageSpotEditor</name>
    <message>
        <location filename="../Sources/filters/imagespot/ptImageSpotEditor.cpp" line="49"/>
        <source>Delete this spot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptImageSpotModel</name>
    <message>
        <location filename="../Sources/filters/imagespot/ptImageSpotModel.cpp" line="167"/>
        <source>%1
x=%2, y=%3 (1:1 pipe size)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptImageView</name>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="105"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="105"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="109"/>
        <source>Zoom &amp;100%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="109"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="113"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="113"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="117"/>
        <source>Zoom &amp;fit</source>
        <translation type="unfinished">Zoom adapté</translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="117"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filemgmt/ptImageView.cpp" line="318"/>
        <source>Fit</source>
        <translation>Adapter</translation>
    </message>
</context>
<context>
    <name>ptJobListModel</name>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="88"/>
        <source>Waiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="89"/>
        <source>Processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="90"/>
        <source>Finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="91"/>
        <source>Skipped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="92"/>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="264"/>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="280"/>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="284"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="93"/>
        <source>Aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="118"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="119"/>
        <source>Output path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="120"/>
        <source>Output suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="121"/>
        <source>Input files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="122"/>
        <source>Status</source>
        <translation type="unfinished">État</translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="198"/>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="314"/>
        <source>Invalid settings file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="199"/>
        <source>
is not a Photivo settings file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="234"/>
        <source>Remove this job?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="235"/>
        <source>
job is being processed. Do you want to abort processing and remove it from the list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="264"/>
        <source>Error writing job list file
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="280"/>
        <source>Error reading job list file
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="284"/>
        <source>
is not a job list file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/batch/ptJobListModel.cpp" line="315"/>
        <source>
doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptMainWindow</name>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="222"/>
        <source>Tool search</source>
        <translation>Recherche d&apos;outil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="245"/>
        <source>Reset</source>
        <translation>Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="278"/>
        <location filename="../Sources/ptMainWindow.cpp" line="1818"/>
        <source>Favourite tools</source>
        <translation>Outils favoris</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="298"/>
        <source>All visible tools</source>
        <translation>Tous les outils visibles</translation>
    </message>
    <message>
        <source>All tools</source>
        <translation type="obsolete">Tous les outils</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="318"/>
        <source>Active tools</source>
        <translation>Outils actifs</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="391"/>
        <location filename="../Sources/ptMainWindow.ui" line="1483"/>
        <location filename="../Sources/ptMainWindow.ui" line="5721"/>
        <source>Camera</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="430"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="506"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <source>WP</source>
        <translation type="obsolete">PB</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="534"/>
        <location filename="../Sources/ptMainWindow.ui" line="579"/>
        <location filename="../Sources/ptMainWindow.ui" line="596"/>
        <source>Open settings file</source>
        <translation>Ouvrir un fichier de configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="551"/>
        <location filename="../Sources/ptMainWindow.cpp" line="522"/>
        <source>Open preset</source>
        <translation>Ouvrir un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="683"/>
        <source>Camera color space</source>
        <translation>Espace de couleurs de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="703"/>
        <location filename="../Sources/ptMainWindow.ui" line="6520"/>
        <location filename="../Sources/ptMainWindow.ui" line="6672"/>
        <source>Load a camera profile</source>
        <translation>Charger un profil d&apos;appareil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4588"/>
        <location filename="../Sources/ptMainWindow.ui" line="6309"/>
        <location filename="../Sources/ptMainWindow.ui" line="6403"/>
        <location filename="../Sources/ptMainWindow.ui" line="6523"/>
        <location filename="../Sources/ptMainWindow.ui" line="6675"/>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4980"/>
        <source>Suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5002"/>
        <source>Suffix to be added to the end of the output file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6503"/>
        <source>Export command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7876"/>
        <source>Load template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7896"/>
        <source>Save image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7916"/>
        <source>Send image to external editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8019"/>
        <source>Processing pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8042"/>
        <source>Info pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8065"/>
        <source>Photivo settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="782"/>
        <source>Generic Corrections</source>
        <translation>Corrections génériques</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="20"/>
        <source>Photivo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="809"/>
        <source>Darkframe image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="850"/>
        <source>DcRaw bad pixel file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="901"/>
        <source>White Balance</source>
        <translation>Balance des blancs</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="937"/>
        <source>Spot white balance</source>
        <translation>Pointer la balance de blanc</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1164"/>
        <source>Demosaicing</source>
        <translation>Dématriçage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1321"/>
        <source>Highlight recovery</source>
        <translation>Récupération des hautes lumières</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1367"/>
        <source>Local Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1420"/>
        <source>Geometry</source>
        <translation>Géométrie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1459"/>
        <source>Lens Parameters (Lensfun)</source>
        <translation>Paramètres de l&apos;objectif (Lensfun)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1515"/>
        <location filename="../Sources/ptMainWindow.ui" line="5779"/>
        <source>Lens</source>
        <translation>Objectif</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1637"/>
        <source>Chromatic Aberration (Lensfun)</source>
        <translation>Aberrations chromatiques (Lensfun)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1661"/>
        <location filename="../Sources/ptMainWindow.ui" line="1796"/>
        <location filename="../Sources/ptMainWindow.ui" line="1898"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1772"/>
        <source>Vignetting (Lensfun)</source>
        <translation>Vignettage (Lensfun)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="1874"/>
        <source>Lens Distortion (Lensfun)</source>
        <translation>Distorsion d&apos;objectif (Lensfun)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2042"/>
        <source>Geometry Conversion (Lensfun)</source>
        <translation>Conversion de géométrie (Lensfun)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2066"/>
        <source>Source geometry</source>
        <translation>Géométrie source</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2107"/>
        <source>Target geometry</source>
        <translation>Géométrie cible</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2158"/>
        <source>Defish</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2204"/>
        <source>Rotation and Perspective</source>
        <translation>Rotation et perspective</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2228"/>
        <source>Rotate left</source>
        <translation>Rotation à gauche</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2245"/>
        <source>Rotate right</source>
        <translation>Rotation à droite</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2278"/>
        <source>Get rotate angle</source>
        <translation>Obtenir l&apos;angle de rotation</translation>
    </message>
    <message>
        <source>Rotate angle</source>
        <translation type="obsolete">Angle de rotation</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2458"/>
        <location filename="../Sources/ptMainWindow.ui" line="6825"/>
        <source>Crop</source>
        <translation>Découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2504"/>
        <source>Make a crop</source>
        <translation>Découper</translation>
    </message>
    <message>
        <source>Crop area</source>
        <translation type="obsolete">Zone de découpe</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2524"/>
        <source>Confirm crop</source>
        <translation>Confirmer le découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2541"/>
        <source>Cancel crop</source>
        <translation>Annuler le découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2574"/>
        <source>Switch crop between portrait/landscape</source>
        <translation>Basculer entre découpage portrait/paysage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2585"/>
        <source>Center horizontally</source>
        <translation>Centrer horizontalement</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2596"/>
        <source>Center vertically</source>
        <translation>Centrer verticalement</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2660"/>
        <source>W</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2692"/>
        <source>H </source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2735"/>
        <source>Guidelines</source>
        <translation>Lignes guides</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2745"/>
        <source>Lights Out</source>
        <translation>Éclairage à l&apos;extérieur</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2798"/>
        <source>Seam carving (*)</source>
        <translation>Recadrage intelligent (*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2828"/>
        <source>Scaling</source>
        <translation>Redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="2944"/>
        <source>Resize</source>
        <translation>Redimensionner</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3034"/>
        <source>Flip</source>
        <translation>Retournement</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3071"/>
        <source>Block</source>
        <translation>Bloquer</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3108"/>
        <source>RGB</source>
        <translation>RVB</translation>
    </message>
    <message>
        <source>Channel  Mixer</source>
        <translation type="vanished">Mixeur de canaux</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8769"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6423"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="obsolete">Hautes lumières</translation>
    </message>
    <message>
        <source>Color Intensity</source>
        <translation type="obsolete">Intensité de couleur</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="obsolete">Luminosité</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5661"/>
        <source>Exposure</source>
        <translation>Exposition</translation>
    </message>
    <message>
        <source>Brighten</source>
        <translation type="obsolete">Éclaircir</translation>
    </message>
    <message>
        <source>GammaTool</source>
        <translation type="obsolete">Gamma</translation>
    </message>
    <message>
        <source>Normalization</source>
        <translation type="obsolete">Normalisation</translation>
    </message>
    <message>
        <source>Color Enhancement</source>
        <translation type="obsolete">Amélioration de couleur</translation>
    </message>
    <message>
        <source>Low/Mid/Highlight Recovery</source>
        <translation type="obsolete">Récupération des tons Sombres/Moyens/Clairs</translation>
    </message>
    <message>
        <source>Texture Contrast</source>
        <translation type="vanished">Contraste des textures</translation>
    </message>
    <message>
        <source>Local Contrast I</source>
        <translation type="vanished">Contraste local I</translation>
    </message>
    <message>
        <source>Local Contrast II</source>
        <translation type="vanished">Contraste local II</translation>
    </message>
    <message>
        <source>RGB Contrast</source>
        <translation type="obsolete">Contraste RVB</translation>
    </message>
    <message>
        <source>Increase RGB contrast by a sigmoidal curve</source>
        <translation type="obsolete">Augmenter le contraste RVB avec une courbe en S</translation>
    </message>
    <message>
        <source>Levels</source>
        <translation type="obsolete">Niveaux</translation>
    </message>
    <message>
        <source>RGB Curve</source>
        <translation type="obsolete">Courbe RVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3373"/>
        <source>Lab Color/Contrast</source>
        <translation>Couleur/Contraste Lab</translation>
    </message>
    <message>
        <source>LAB Transform</source>
        <translation type="obsolete">Transformation Lab</translation>
    </message>
    <message>
        <source>Shadows / Highlights</source>
        <translation type="obsolete">Ombres / Hautes lumières</translation>
    </message>
    <message>
        <source>Dynamic Range Compression (*)</source>
        <translation type="obsolete">Compression à gamme dynamique (*)</translation>
    </message>
    <message>
        <source>Texture curve (*)</source>
        <translation type="obsolete">Courbe de texture (*)</translation>
    </message>
    <message>
        <source>Texture Contrast I</source>
        <translation type="vanished">Contraste de texture I</translation>
    </message>
    <message>
        <source>Texture Contrast II</source>
        <translation type="vanished">Contraste de texture II</translation>
    </message>
    <message>
        <source>Local Contrast Stretch I (*)</source>
        <translation type="vanished">Étirement de contraste I (*)</translation>
    </message>
    <message>
        <source>Local Contrast Stretch II (*)</source>
        <translation type="vanished">Étirement de contraste II (*)</translation>
    </message>
    <message>
        <source>Lightness Contrast</source>
        <translation type="obsolete">Contraste de luminosité</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3569"/>
        <source>Increase contrast on L by a sigmoidal curve</source>
        <translation>Augmenter le contraste sur L avec une courbe en S</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3586"/>
        <source>Increase saturation by a sigmoidal curve</source>
        <translation>Augmente la saturation avec une courbe en S</translation>
    </message>
    <message>
        <source>Color Boost</source>
        <translation type="obsolete">Renforcement de couleur</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3628"/>
        <source>Lab Sharpen/Noise</source>
        <translation>Accentuation/Bruit Lab</translation>
    </message>
    <message>
        <source>Impulse noise reduction</source>
        <translation type="vanished">Réduction du bruit d&apos;impulsion</translation>
    </message>
    <message>
        <source>Edge avoiding wavelets</source>
        <translation type="vanished">Ondelettes à évitement de bords</translation>
    </message>
    <message>
        <source>GreyCStoration on L (*)</source>
        <translation type="vanished">GreyCStoration sur L (*)</translation>
    </message>
    <message>
        <source>Defringe</source>
        <translation type="vanished">Détourage</translation>
    </message>
    <message>
        <source>Wavelet Denoising</source>
        <translation type="vanished">Débruitage par ondelettes</translation>
    </message>
    <message>
        <source>Luminance Denoising (*)</source>
        <translation type="vanished">Débruitage de la luminance (*)</translation>
    </message>
    <message>
        <source>Luminance Denoise Curve (*)</source>
        <translation type="obsolete">Courbe de débruitage de la luminance (*)</translation>
    </message>
    <message>
        <source>Pyramid Denoising</source>
        <translation type="vanished">Débruitage pyramidal</translation>
    </message>
    <message>
        <source>Color Denoising (*)</source>
        <translation type="vanished">Débruitage de couleur (*)</translation>
    </message>
    <message>
        <source>Detail curve</source>
        <translation type="obsolete">Courbe de détail</translation>
    </message>
    <message>
        <source>Gradient Sharpen</source>
        <translation type="vanished">Accentuation par gradient</translation>
    </message>
    <message>
        <source>Wiener Filter (Sharpen) (*)</source>
        <translation type="obsolete">Filtre de Wiener (Accentuation) (*)</translation>
    </message>
    <message>
        <source>Inverse Diffusion Sharpen</source>
        <translation type="vanished">Accentuation par diffusion inverse</translation>
    </message>
    <message>
        <source>Unsharp Mask (USM)</source>
        <translation type="vanished">Masque flou</translation>
    </message>
    <message>
        <source>Highpass Sharpen</source>
        <translation type="vanished">Accentuation passe-haut</translation>
    </message>
    <message>
        <source>Film grain simulation</source>
        <translation type="vanished">Simulation de grain de film</translation>
    </message>
    <message>
        <source>View LAB</source>
        <translation type="vanished">Vue en Lab</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="3918"/>
        <source>Lab EyeCandy</source>
        <translation>Décoration Lab</translation>
    </message>
    <message>
        <source>Luminance by hue curve</source>
        <translation type="obsolete">Luminance par courbe de teinte</translation>
    </message>
    <message>
        <source>Saturation curve</source>
        <translation type="obsolete">Courbe de saturation</translation>
    </message>
    <message>
        <source>Hue curve</source>
        <translation type="obsolete">Courbe de teinte</translation>
    </message>
    <message>
        <source>L* curve</source>
        <translation type="obsolete">Courbe L*</translation>
    </message>
    <message>
        <source>a* b* curves</source>
        <translation type="obsolete">Courbes a* b*</translation>
    </message>
    <message>
        <source>Color contrast</source>
        <translation type="obsolete">Contraste de couleur</translation>
    </message>
    <message>
        <source>Tone adjustment I</source>
        <translation type="obsolete">Ajustement des tons I</translation>
    </message>
    <message>
        <source>Tone adjustment II</source>
        <translation type="obsolete">Ajustement des tons II</translation>
    </message>
    <message>
        <source>Luminance adjustment</source>
        <translation type="obsolete">Ajustement de la luminance</translation>
    </message>
    <message>
        <source>Saturation adjustment</source>
        <translation type="obsolete">Ajustement de la saturation</translation>
    </message>
    <message>
        <source>Tone</source>
        <translation type="obsolete">Tons</translation>
    </message>
    <message>
        <source>All values</source>
        <translation type="obsolete">Toutes les valeurs</translation>
    </message>
    <message>
        <source>Shadows</source>
        <translation type="obsolete">Ombres</translation>
    </message>
    <message>
        <source>Midtones</source>
        <translation type="obsolete">Tons moyens</translation>
    </message>
    <message>
        <source>Lights</source>
        <translation type="obsolete">Tons clairs</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4142"/>
        <source>EyeCandy</source>
        <translation>Décoration</translation>
    </message>
    <message>
        <source>Black and White</source>
        <translation type="vanished">Noir et blanc</translation>
    </message>
    <message>
        <source>Simple tone</source>
        <translation type="vanished">Réglage simple des tons</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7090"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Cross processing</source>
        <translation type="vanished">Traitement croisé</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="obsolete">Contraste</translation>
    </message>
    <message>
        <source>Texture overlay</source>
        <translation type="obsolete">Calque de texture</translation>
    </message>
    <message>
        <source>Clear image</source>
        <translation type="vanished">Effacer l&apos;image</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Effacer</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="489"/>
        <source>Load image</source>
        <translation>Charger une image</translation>
    </message>
    <message>
        <source>Gradual overlay I</source>
        <translation type="vanished">Calque dégradé I</translation>
    </message>
    <message>
        <source>Gradual overlay II</source>
        <translation type="vanished">Calque dégradé II</translation>
    </message>
    <message>
        <source>Softglow / Orton</source>
        <translation type="vanished">Filtre adoucissant / Orton</translation>
    </message>
    <message>
        <source>R tone curve</source>
        <translation type="obsolete">Courbe de ton R</translation>
    </message>
    <message>
        <source>G tone curve</source>
        <translation type="obsolete">Courbe de ton V</translation>
    </message>
    <message>
        <source>B tone curve</source>
        <translation type="obsolete">Courbe de ton B</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4433"/>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <source>Basecurve</source>
        <translation type="obsolete">Courbe de base</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4489"/>
        <source>sRGB gamma compensation</source>
        <translation>Compensation du gamma sRVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4532"/>
        <source>Output Colorspace</source>
        <translation>Espace de couleurs de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4552"/>
        <source>Reset profile</source>
        <translation>RàZ du profil</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4555"/>
        <location filename="../Sources/ptMainWindow.ui" line="8564"/>
        <source>Full</source>
        <translation>Plein</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4575"/>
        <source>Embedded profile</source>
        <translation>Profil embarqué</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4585"/>
        <source>Load icc profile</source>
        <translation>Charger un profil icc</translation>
    </message>
    <message>
        <source>After gamma curve</source>
        <translation type="obsolete">Courbe post-gamma</translation>
    </message>
    <message>
        <source>Sigmoidal contrast</source>
        <translation type="obsolete">Contraste en S</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4676"/>
        <source>Resize for web</source>
        <translation>Redimensionnement pour le web</translation>
    </message>
    <message>
        <source>Wiener Filter (Sharpen)</source>
        <translation type="obsolete">Filtre de Wiener (Accentuation)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="4810"/>
        <source>Output Parameters</source>
        <translation>Paramètres de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5075"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5161"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5244"/>
        <source>Tags</source>
        <translation>Étiquettes</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5288"/>
        <source>Tags for the photo (IPTC and XMP)</source>
        <translation>Étiquettes pour la photo (IPTC et XMP)</translation>
    </message>
    <message>
        <source>Write the output file</source>
        <translation type="obsolete">Écrire le fichier de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5360"/>
        <source>About Photivo</source>
        <translation>À propos de Photivo</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5444"/>
        <source>Photivo photo processor</source>
        <translation>Logiciel de traitement photo Photivo</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5454"/>
        <location filename="../Sources/ptMainWindow.ui" line="8350"/>
        <source>Rev:</source>
        <translation>Rév:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5464"/>
        <source>&lt;a href=&quot;https://photivo.org&quot;&gt;photivo.org&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5518"/>
        <source>File info</source>
        <translation>Infos sur le fichier</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5554"/>
        <source>File name:</source>
        <translation>Nom du fichier:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5561"/>
        <source>File name of the currently loaded image</source>
        <translation>Nom de fichier de l&apos;image courante</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5601"/>
        <source>Path:</source>
        <translation>Chemin:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5608"/>
        <source>Path of the currently loaded image</source>
        <translation>Chemin de l&apos;image courante</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5646"/>
        <source>Exif info</source>
        <translation>Infos Exif</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5692"/>
        <source>Focal length</source>
        <translation>Longueur focale</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5810"/>
        <source>Time of original</source>
        <translation>Date de l&apos;original</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5838"/>
        <source>Flash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5866"/>
        <source>Whitebalance</source>
        <translation>Balance des blancs</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5894"/>
        <source>dcraw info</source>
        <translation>Infos dcraw</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5941"/>
        <source>Size info</source>
        <translation>Infos sur la taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5977"/>
        <source>Input:</source>
        <translation>Entrée:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="5989"/>
        <source>Size of the image at the beginning</source>
        <translation>Taille de l&apos;image au début</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6029"/>
        <source>1:1 pipe:</source>
        <translation>Tube 1:1:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6041"/>
        <source>Size of the image at the end of the full pipe (without webresize)</source>
        <translation>Taille de l&apos;image à la fin du tube complet (sans redimensionnement web)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6081"/>
        <source>Current:</source>
        <translation>Courante:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6093"/>
        <source>Size of the image at the end of the current pipe (without webresize)</source>
        <translation>Taille de l&apos;image à la fin du tube actuel (sans redimensionneent web)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6156"/>
        <source>TextLabel</source>
        <translation>Étiquette textuelle</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6236"/>
        <source>Work Colorspace</source>
        <translation>Espace de couleurs de travail</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6273"/>
        <source>Preview Colorspace</source>
        <translation>Espace de couleurs d&apos;aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6306"/>
        <source>Load an icc profile</source>
        <translation>Charger un profil ICC</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6389"/>
        <source>UI settings</source>
        <translation>Configuration de l&apos;IHM</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6400"/>
        <source>Load UI settings from file</source>
        <translation>Charger la configuration de l&apos;IHM à partir d&apos;un fichier</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6420"/>
        <source>Save UI settings to file</source>
        <translation>Enregistrer la configuration de l&apos;IHM dans un fichier</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6453"/>
        <source>Discard all changes</source>
        <translation>Abandonner tous les changements</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6456"/>
        <source>Discard</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <source>Gimp command</source>
        <translation type="obsolete">Commande pour Gimp</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6599"/>
        <source>Remember settings</source>
        <translation>Se souvenir de la configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6636"/>
        <source>Startup settings</source>
        <translation>Configuration au démarrage</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6732"/>
        <source>Tool pane mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6762"/>
        <source>Pipe size</source>
        <translation>Taille du tube</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6849"/>
        <source>Initial zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6900"/>
        <source>Inputs</source>
        <translation>Entrées</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6937"/>
        <source>ToolBoxes</source>
        <translation>Boîtes à outils</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="6974"/>
        <source>Tab status indicator</source>
        <translation>Indicateur d&apos;état des onglets</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7011"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7048"/>
        <source>Theming</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7140"/>
        <source>Set new CSS style</source>
        <translation>Définir un nouveau style CSS</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7143"/>
        <source>CSS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7210"/>
        <source>Button modes</source>
        <translation>Modes des boutons</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7258"/>
        <source>Save button:</source>
        <translation>Bouton d&apos;enreg. :</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7310"/>
        <source>Reset button:</source>
        <translation>Boutons de RàZ :</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7348"/>
        <source>Searchbar</source>
        <translation>Barre de recherche</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7385"/>
        <source>Show confirmation dialogs</source>
        <translation>Afficher les dialogues de confirmations</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7451"/>
        <source>Backup settings</source>
        <translation>Paramètres de sauvegarde</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7488"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7515"/>
        <source>Switch Photivo&apos;s language</source>
        <translation>Changer la langue de Photivo</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7576"/>
        <source>Memory test</source>
        <translation>Test mémoire</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7613"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7685"/>
        <source>Batch Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7770"/>
        <source>Change preview mode</source>
        <translation>Changer le mode d&apos;aperçu</translation>
    </message>
    <message>
        <source>Preview mode</source>
        <translation type="obsolete">Mode d&apos;aperçu</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7837"/>
        <source>Run pipe</source>
        <translation>Exécuter le tube</translation>
    </message>
    <message>
        <source>Run</source>
        <translation type="obsolete">Lancer</translation>
    </message>
    <message>
        <source>Write output to a file</source>
        <translation type="obsolete">Écrire la sortie dans un fichier</translation>
    </message>
    <message>
        <source>Send to Gimp</source>
        <translation type="obsolete">Envoyer dans Gimp</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="obsolete">Exporter</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="7993"/>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Show processing</source>
        <translation type="obsolete">Afficher le traitement</translation>
    </message>
    <message>
        <source>Show settings</source>
        <translation type="obsolete">Afficher la configuration</translation>
    </message>
    <message>
        <source>Show info</source>
        <translation type="obsolete">Afficher les infos</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8302"/>
        <source>Open image</source>
        <translation>Ouvrir une image</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8340"/>
        <source>&lt;p align=&quot;center&quot;&gt;Photivo photo processor&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Logiciel de traitement photo Photivo&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8360"/>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://photivo.org&quot;&gt;photivo.org&lt;/a&gt;&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8373"/>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.flickr.com/groups/photivo/discuss/&quot;&gt;Forum&lt;/a&gt; &lt;a href=&quot;https://photivo.org/download/&quot;&gt;Updates&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://www.flickr.com/groups/photivo/discuss/&quot;&gt;Forum&lt;/a&gt; &lt;a href=&quot;https://photivo.org/download/&quot;&gt;Updates&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8486"/>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8503"/>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8541"/>
        <source>Zoom fit</source>
        <translation>Zoom adapté</translation>
    </message>
    <message>
        <source>Fit</source>
        <translation type="obsolete">Adapter</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8561"/>
        <source>Zoom Full</source>
        <translation>Taille réelle</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8603"/>
        <source>Open previous image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8620"/>
        <source>Open next image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8653"/>
        <source>Batch job processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8670"/>
        <source>File manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8687"/>
        <source>Fullscreen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8774"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8782"/>
        <source>TabMode</source>
        <translation>Onglets</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8787"/>
        <source>Save Output</source>
        <translation>Enregistrer la sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8792"/>
        <source>Write Job</source>
        <translation>Tâche d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8800"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.ui" line="8811"/>
        <source>Add power law</source>
        <translation>Ajouter une règle de puissance</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="500"/>
        <source>Save current pipe</source>
        <translation>Enregistrer le tube courant</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="502"/>
        <source>Save full size</source>
        <translation>Enregistrer en pleine taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="504"/>
        <source>Save settings file</source>
        <translation>Enregistrer le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="506"/>
        <source>Save job file</source>
        <translation>Enregistrer le fichier de tâches</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="508"/>
        <source>Send to batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="512"/>
        <source>Export current pipe</source>
        <translation>Exporter le tube courant</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="514"/>
        <source>Export full size</source>
        <translation>Exporter en pleine taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="518"/>
        <source>Neutral reset</source>
        <translation>RàZ neutre</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="520"/>
        <source>User reset</source>
        <translation>RàZ utilisateur</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="524"/>
        <source>Open settings</source>
        <translation>Ouvrir une configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="528"/>
        <source>&amp;Show hidden tools</source>
        <translation>Afficher les outils masqués</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="541"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="624"/>
        <source>English (Default)</source>
        <translation>Anglais (Défaut)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="633"/>
        <source>Restart Photivo to change the language.</source>
        <translation>Redémarrer Photivo pour modifier la langue.</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1793"/>
        <source>No tools visible!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings file dropped!</source>
        <translation type="obsolete">Fichier de configuration abandonné!</translation>
    </message>
    <message>
        <source>Do you really want to open
</source>
        <translation type="obsolete">Souhaitez-vous vraiment ouvrir</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1629"/>
        <source>No tools hidden!</source>
        <translation>Pas d&apos;outils masqués!</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1630"/>
        <source>Hidden tools</source>
        <translation>Outils masqués</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1648"/>
        <source>No tools blocked!</source>
        <translation>Pas d&apos;outils bloqués!</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1649"/>
        <source>Blocked tools</source>
        <translation>Outils bloqués</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1746"/>
        <source>Search results:</source>
        <translation>Résultats de la recherche:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1768"/>
        <source>No tools active!</source>
        <translation>Pas d&apos;outils actifs!</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1770"/>
        <source>Active tools:</source>
        <translation>Outils actifs:</translation>
    </message>
    <message>
        <source>All tools hidden</source>
        <translation type="obsolete">Tous les outils sont masqués</translation>
    </message>
    <message>
        <source>No visible tools!</source>
        <translation type="obsolete">Pas d&apos;outils visibles!</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1795"/>
        <source>All visible tools:</source>
        <translation>Tous les outils visibles:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1818"/>
        <source>No favourite tools!</source>
        <translation>Pas d&apos;outils favoris!</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="1822"/>
        <source>Favourite tools:</source>
        <translation>Outils favoris:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2338"/>
        <source> at </source>
        <translation> à </translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2357"/>
        <source> with ISO </source>
        <translation> à ISO </translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2398"/>
        <source> (35mm equiv.: </source>
        <translation>(Équiv. 35mm:</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2799"/>
        <location filename="../Sources/ptMainWindow.cpp" line="2823"/>
        <source>Photivo UI file (*.ptu);;All files (*.*)</source>
        <translation>Fichier IHM de Photivo (*.ptu);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2801"/>
        <source>Open UI</source>
        <translation>Ouvir une IHM</translation>
    </message>
    <message>
        <location filename="../Sources/ptMainWindow.cpp" line="2825"/>
        <source>Save UI</source>
        <translation>Enregistrer l&apos;IHM</translation>
    </message>
</context>
<context>
    <name>ptProcessor</name>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="183"/>
        <source>Loading Bitmap</source>
        <translation>Charger un bitmap</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="222"/>
        <location filename="../Sources/ptProcessor.cpp" line="253"/>
        <source>Reading exif info</source>
        <translation>Lecture des info exif</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="236"/>
        <source>Reading RAW file</source>
        <translation>Lecture du fichier Raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="264"/>
        <source>Demosaicing</source>
        <translation>Dématriçage</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="274"/>
        <source>Recovering highlights</source>
        <translation>Récupération des hautes lumières</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="296"/>
        <source>Profile not found</source>
        <translation>Profil introuvable</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="297"/>
        <source>Profile not found. Reverting to Adobe Matrix.
You could try an external profile.</source>
        <translation>Profil introuvable. Retour à une matrice Adobe.
Vous pouvez aussi essayer un profil externe.</translation>
    </message>
    <message>
        <source>Calculate auto exposure</source>
        <translation type="vanished">Calculer l&apos;exposition auto</translation>
    </message>
    <message>
        <source>Channel Mixing</source>
        <translation type="vanished">Mixage de canaux</translation>
    </message>
    <message>
        <source>IntensityRGB</source>
        <translation type="obsolete">Intensité RVB</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="obsolete">Luminosité</translation>
    </message>
    <message>
        <source>Correcting Exposure</source>
        <translation type="vanished">Correction de l&apos;exposition</translation>
    </message>
    <message>
        <source>Brighten</source>
        <translation type="obsolete">Éclaircir</translation>
    </message>
    <message>
        <source>Applying RGB Gamma</source>
        <translation type="obsolete">Application du gamma RVB</translation>
    </message>
    <message>
        <source>Normalization</source>
        <translation type="obsolete">Normalisation</translation>
    </message>
    <message>
        <source>Color enhance</source>
        <translation type="obsolete">Amélioration de couleur</translation>
    </message>
    <message>
        <source>Local Exposure</source>
        <translation type="obsolete">Exposition locale</translation>
    </message>
    <message>
        <source>RGB Texture contrast</source>
        <translation type="vanished">Contraste de texture RVB</translation>
    </message>
    <message>
        <source>Microcontrast 1</source>
        <translation type="vanished">Micro-contraste 1</translation>
    </message>
    <message>
        <source>Microcontrast 2</source>
        <translation type="vanished">Micro-contraste 2</translation>
    </message>
    <message>
        <source>Luminance adjustment</source>
        <translation type="obsolete">Ajustement de la luminance</translation>
    </message>
    <message>
        <source>Saturation adjustment</source>
        <translation type="obsolete">Ajustement de la saturation</translation>
    </message>
    <message>
        <source>Applying RGB Contrast</source>
        <translation type="vanished">Application du contraste RVB</translation>
    </message>
    <message>
        <source>Levels</source>
        <translation type="obsolete">Niveaux</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation type="obsolete">Hautes lumières</translation>
    </message>
    <message>
        <source>Applying RGB curve</source>
        <translation type="obsolete">Application de la courbe RVB</translation>
    </message>
    <message>
        <source>Lab transform</source>
        <translation type="obsolete">Transformation Lab</translation>
    </message>
    <message>
        <source>Shadows and Highlights</source>
        <translation type="obsolete">Ombres et hautes lumières</translation>
    </message>
    <message>
        <source>LabLocal Exposure</source>
        <translation type="obsolete">Exposition locale Lab</translation>
    </message>
    <message>
        <source>Dynamic Range Compression</source>
        <translation type="obsolete">Compression à gamme dynamique</translation>
    </message>
    <message>
        <source>Texture curve</source>
        <translation type="vanished">Courbe de texture</translation>
    </message>
    <message>
        <source>Texture contrast 1</source>
        <translation type="vanished">Contraste de texture 1</translation>
    </message>
    <message>
        <source>Texture contrast 2</source>
        <translation type="vanished">Contraste de texture 2</translation>
    </message>
    <message>
        <source>LabMicrocontrast 1</source>
        <translation type="vanished">Micro-contraste Lab 1</translation>
    </message>
    <message>
        <source>LabMicrocontrast 2</source>
        <translation type="vanished">Micro-contraste Lab 2</translation>
    </message>
    <message>
        <source>Local Contrast 1</source>
        <translation type="vanished">Contraste local 1</translation>
    </message>
    <message>
        <source>Local Contrast 2</source>
        <translation type="vanished">Contraste local 2</translation>
    </message>
    <message>
        <source>Applying Lab contrast</source>
        <translation type="obsolete">Application du contraste Lab</translation>
    </message>
    <message>
        <source>Applying Lab saturation</source>
        <translation type="obsolete">Application de la saturation Lab</translation>
    </message>
    <message>
        <source>Applying Color Boost</source>
        <translation type="obsolete">Application du renforcement de couleur</translation>
    </message>
    <message>
        <source>LabLevels</source>
        <translation type="obsolete">Niveaux Lab</translation>
    </message>
    <message>
        <source>Impulse denoise</source>
        <translation type="vanished">Débruitage des impulsions</translation>
    </message>
    <message>
        <source>Edge avoiding wavelets</source>
        <translation type="vanished">Ondelettes à évitement de bords</translation>
    </message>
    <message>
        <source>GreyCStoration on L</source>
        <translation type="vanished">GreyCStoration sur L</translation>
    </message>
    <message>
        <source>Defringe</source>
        <translation type="vanished">Détourage</translation>
    </message>
    <message>
        <source>Wavelet L denoising</source>
        <translation type="vanished">Débruitage par ondelettes sur L</translation>
    </message>
    <message>
        <source>Wavelet A denoising</source>
        <translation type="vanished">Débruitage par ondelettes sur A</translation>
    </message>
    <message>
        <source>Wavelet B denoising</source>
        <translation type="vanished">Débruitage par ondelettes sur A</translation>
    </message>
    <message>
        <source>Luminance denoising</source>
        <translation type="vanished">Débruitage de la luminance</translation>
    </message>
    <message>
        <source>Denoise curve</source>
        <translation type="vanished">Courbe de débruitage</translation>
    </message>
    <message>
        <source>Pyramid denoising</source>
        <translation type="vanished">Débruitage pyramidal</translation>
    </message>
    <message>
        <source>Color A denoising</source>
        <translation type="vanished">Débruitage de la couleur A</translation>
    </message>
    <message>
        <source>Color B denoising</source>
        <translation type="vanished">Débruitage de la couleur B</translation>
    </message>
    <message>
        <source>Detail curve</source>
        <translation type="vanished">Courbe de détail</translation>
    </message>
    <message>
        <source>Gradient Sharpen</source>
        <translation type="vanished">Accentuation par gradient</translation>
    </message>
    <message>
        <source>Wiener Filter</source>
        <translation type="vanished">Filtre de Wiener</translation>
    </message>
    <message>
        <source>Inverse Diffusion Sharpen</source>
        <translation type="vanished">Accentuation par diffusion inverse</translation>
    </message>
    <message>
        <source>USM sharpening</source>
        <translation type="vanished">Accentuation par masque flou</translation>
    </message>
    <message>
        <source>Highpass</source>
        <translation type="vanished">Passe-haut</translation>
    </message>
    <message>
        <source>Film grain 1</source>
        <translation type="vanished">Grain de film 1</translation>
    </message>
    <message>
        <source>Film grain 2</source>
        <translation type="vanished">Grain de film 2</translation>
    </message>
    <message>
        <source>View LAB</source>
        <translation type="vanished">Vue Lab</translation>
    </message>
    <message>
        <source>Applying L by Hue curve</source>
        <translation type="vanished">Application de L par la courbe de teinte</translation>
    </message>
    <message>
        <source>Applying saturation curve</source>
        <translation type="vanished">Application de la courbe de saturation</translation>
    </message>
    <message>
        <source>Applying hue curve</source>
        <translation type="vanished">Application de la courbe de teinte</translation>
    </message>
    <message>
        <source>Applying L curve</source>
        <translation type="vanished">Application de la courbe L</translation>
    </message>
    <message>
        <source>Applying a curve</source>
        <translation type="obsolete">Application de la courbe a</translation>
    </message>
    <message>
        <source>Applying b curve</source>
        <translation type="obsolete">Application de la courbe b</translation>
    </message>
    <message>
        <source>Colorcontrast</source>
        <translation type="obsolete">Contraste de couleurs</translation>
    </message>
    <message>
        <source>LAB tone adjustments 1</source>
        <translation type="obsolete">Ajustement de ton Lab 1</translation>
    </message>
    <message>
        <source>LAB tone adjustments 2</source>
        <translation type="obsolete">Ajustement de ton Lab 2</translation>
    </message>
    <message>
        <source>Luminance and saturation adjustment</source>
        <translation type="obsolete">Ajustement de la luminance et de la saturation</translation>
    </message>
    <message>
        <source>LAB toning</source>
        <translation type="obsolete">Réglage de ton Lab</translation>
    </message>
    <message>
        <source>LAB shadows toning</source>
        <translation type="obsolete">Réglage de tons sombres Lab</translation>
    </message>
    <message>
        <source>LAB midtones toning</source>
        <translation type="obsolete">Réglage de tons moyens Lab</translation>
    </message>
    <message>
        <source>LAB highlights toning</source>
        <translation type="obsolete">Réglage de tons clairs Lab</translation>
    </message>
    <message>
        <source>Lab Vignette</source>
        <translation type="vanished">Vignette Lab</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1000"/>
        <source>Lab to RGB</source>
        <translation>Lab vers RVB</translation>
    </message>
    <message>
        <source>Black and White</source>
        <translation type="vanished">Noir et blanc</translation>
    </message>
    <message>
        <source>Simple Toning</source>
        <translation type="vanished">Réglage simple des tons</translation>
    </message>
    <message>
        <source>Toning</source>
        <translation type="obsolete">Réglage des tons</translation>
    </message>
    <message>
        <source>Crossprocessing</source>
        <translation type="vanished">Traitement croisé</translation>
    </message>
    <message>
        <source>Texture Overlay</source>
        <translation type="vanished">Calque de texture</translation>
    </message>
    <message>
        <source>Gradual Overlay 1</source>
        <translation type="vanished">Calque dégradé 1</translation>
    </message>
    <message>
        <source>Gradual Overlay 2</source>
        <translation type="vanished">Calque dégradé 2</translation>
    </message>
    <message>
        <source>Softglow</source>
        <translation type="vanished">Lueur douce</translation>
    </message>
    <message>
        <source>Intensity RGB 2</source>
        <translation type="obsolete">Intensité RVB 2</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1149"/>
        <source>Applying R curve</source>
        <translation>Application de la courbe R</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1156"/>
        <source>Applying G curve</source>
        <translation>Application de la courbe V</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1163"/>
        <source>Applying B curve</source>
        <translation>Application de la courbe B</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1185"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1202"/>
        <source>Transfer Bitmap</source>
        <translation>Tranférer le bitmap</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1252"/>
        <source>Spot tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1277"/>
        <source>Lensfun corrections</source>
        <translation>Corrections Lensfun</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1437"/>
        <source>Defish correction</source>
        <translation>Correction Defish</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1504"/>
        <source>Perspective transform</source>
        <translation>Transformation de perspective</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1547"/>
        <source>Crop outside the image</source>
        <translation>Découpage hors de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1548"/>
        <source>Crop rectangle too large.
No crop, try again.</source>
        <translation>Rectangle de découpe trop grand.
Découpage non effectué. Essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1558"/>
        <source>Cropping</source>
        <translation>Découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1585"/>
        <source>Seam carving</source>
        <translation>Recadrage intelligent</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1608"/>
        <source>Resize image</source>
        <translation>Redimensionner l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptProcessor.cpp" line="1629"/>
        <source>Flip image</source>
        <translation>Retourner l&apos;image</translation>
    </message>
</context>
<context>
    <name>ptSettings</name>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="66"/>
        <source>MB</source>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="66"/>
        <source>MB to waste</source>
        <translation>Mo à perdre</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="67"/>
        <source>Pixel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="67"/>
        <source>Size of the LED</source>
        <translation>Taille de la DEL</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="68"/>
        <source>Maximum slider width</source>
        <translation>Largeur maximale des glissières</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="68"/>
        <source>Maximum slider width. Enter 0 to remove restriction</source>
        <translation>Largeur maximale des glissières. Saisir 0 pour enlever la restriction</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="69"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="69"/>
        <source>Zoom factor</source>
        <translation>Facteur de zoom</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="70"/>
        <source>Temp</source>
        <translation>Temp</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="70"/>
        <source>Color Temperature</source>
        <translation>Température de couleur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="71"/>
        <source>WB-G</source>
        <translation>BdB-V</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="71"/>
        <source>Green Intensity in balance</source>
        <translation>Intensité du vert dans la balance</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="72"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="72"/>
        <source>Red Multiplier in balance</source>
        <translation>Facteur de rouge dans la balance</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="73"/>
        <source>G</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="73"/>
        <source>Green Multiplier in balance</source>
        <translation>Facteur de vert dans la balance</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="74"/>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="74"/>
        <source>Blue Multiplier in balance</source>
        <translation>Facteur de bleu dans la balance</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="75"/>
        <source>BP</source>
        <translation>PN</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="75"/>
        <source>Black point in raw</source>
        <translation>Point noir dans le raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="76"/>
        <source>WP</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="76"/>
        <source>White point in raw</source>
        <translation>Point blanc dans le raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="77"/>
        <source>CA red factor</source>
        <translation>Facteur d&apos;AC rouge</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="78"/>
        <source>CA blue factor</source>
        <translation>Facteur d&apos;AC bleu</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="79"/>
        <source>Green equilibration</source>
        <translation>Équilibrage du vert</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="80"/>
        <source>Line denoise</source>
        <translation>Débruitage en ligne</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="80"/>
        <source>Raw line denoise threshold</source>
        <translation>Seuil de débruitage en ligne du raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="81"/>
        <source>Adjust maximum</source>
        <translation>Ajuster le maximum</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="81"/>
        <source>Threshold to prevent pink highlights</source>
        <translation>Seuil empêchant les hautes lumières roses</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="82"/>
        <source>Wavelet denoise</source>
        <translation>Débruitage en ondelettes</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="82"/>
        <source>Raw wavelet denoise threshold</source>
        <translation>Seuil de débruitage en ondelettes du raw</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="83"/>
        <source>Badpixel reduction</source>
        <translation>Réduction des pixels morts</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="83"/>
        <source>Automatic badpixel reduction</source>
        <translation>Réduction automatique des pixels morts</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="84"/>
        <source>Passes</source>
        <translation>Passes</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="84"/>
        <source>Nr of refinement passes</source>
        <translation>Nb de passes d&apos;amélioration</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="85"/>
        <source>Median passes</source>
        <translation>Passes du filtre médian</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="85"/>
        <source>Nr of median filter passes</source>
        <translation>Nb de passes du filtre médian</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="86"/>
        <source>Edge sensitive median passes</source>
        <translation>Passes du filtre médian à détection de bords</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="86"/>
        <source>Nr of edge sensitive median filter passes</source>
        <translation>Nombre de passes du filtre médian à détection de bords</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="87"/>
        <source>Parameter</source>
        <translation>Paramètre</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="87"/>
        <source>Clip function dependent parameter</source>
        <translation>Paramètre dépendant de la fonction de récupération</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="88"/>
        <location filename="../Sources/ptSettings.cpp" line="113"/>
        <location filename="../Sources/ptSettings.cpp" line="116"/>
        <source>Focal length (35mm equiv.)</source>
        <translation>Longueur focale (équiv. 35mm)</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="89"/>
        <source>Aperture</source>
        <translation>Ouverture</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="90"/>
        <source>Distance</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="90"/>
        <source>Distance between object and camera</source>
        <translation>Distance entre l&apos;objet et l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="91"/>
        <location filename="../Sources/ptSettings.cpp" line="114"/>
        <source>Scale</source>
        <translation>Redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="91"/>
        <source>Image scaling.
Useful to avoid losing content through the distortion/geometry tools.
0.0 means auto-scaling.</source>
        <translation>Redimensionnement de l&apos;image.
Utile pour éviter la perte de contenu à cause des outils de distorsion/géométrie.
0.0 signifie &quot;redimensionnement automatique&quot;.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="92"/>
        <source>kr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="93"/>
        <source>kb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="94"/>
        <source>vr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="95"/>
        <source>vb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="96"/>
        <source>cr</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="97"/>
        <source>cb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="98"/>
        <source>br</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="99"/>
        <source>bb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="100"/>
        <location filename="../Sources/ptSettings.cpp" line="103"/>
        <location filename="../Sources/ptSettings.cpp" line="104"/>
        <source>k1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="101"/>
        <location filename="../Sources/ptSettings.cpp" line="105"/>
        <source>k2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="102"/>
        <source>k3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="107"/>
        <source>omega</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="109"/>
        <source>a</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="110"/>
        <source>b</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="111"/>
        <source>c</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="114"/>
        <source>Image scaling.
0.0 means auto-scaling.</source>
        <translation>Redimensionnement de l&apos;image.
0.0 signifie &quot;redimensionnement auto&quot;.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="115"/>
        <source>Rotate</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="117"/>
        <source>Tilt</source>
        <translation>Inclinaison</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="118"/>
        <source>Turn</source>
        <translation>Retournement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="119"/>
        <location filename="../Sources/ptSettings.cpp" line="124"/>
        <source>Horizontal scale</source>
        <translation>Redimensionnement horizontal</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="120"/>
        <location filename="../Sources/ptSettings.cpp" line="125"/>
        <source>Vertical scale</source>
        <translation>Redimensionnement vertical</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="121"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="121"/>
        <source>Vertical lines</source>
        <translation>Lignes verticales</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="122"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="122"/>
        <source>Horizontal lines</source>
        <translation>Lignes horizontales</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="123"/>
        <source>Crop exposure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="123"/>
        <source>Temporary exposure in EV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="126"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="127"/>
        <location filename="../Sources/ptSettings.cpp" line="129"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="231"/>
        <source>Load tags from sidecar files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="231"/>
        <source>Load tags from sidecar XMP files when opening an image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Taille</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="128"/>
        <location filename="../Sources/ptSettings.cpp" line="132"/>
        <source>Image size</source>
        <translation>Taille de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="132"/>
        <source>pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blackpoint</source>
        <translation type="obsolete">Point noir</translation>
    </message>
    <message>
        <source>Levels Blackpoint</source>
        <translation type="obsolete">Pont noir des niveaux</translation>
    </message>
    <message>
        <source>Whitepoint</source>
        <translation type="obsolete">Point blanc</translation>
    </message>
    <message>
        <source>Levels Whitepoint</source>
        <translation type="obsolete">Point blanc des niveaux</translation>
    </message>
    <message>
        <source>Contribution of red to red</source>
        <translation type="vanished">Contribution du rouge au rouge</translation>
    </message>
    <message>
        <source>Contribution of green to red</source>
        <translation type="vanished">Contribution du vert au rouge</translation>
    </message>
    <message>
        <source>Contribution of blue to red</source>
        <translation type="vanished">Contribution du bleu au rouge</translation>
    </message>
    <message>
        <source>Contribution of red to green</source>
        <translation type="vanished">Contribution du rouge au vert</translation>
    </message>
    <message>
        <source>Contribution of green to green</source>
        <translation type="vanished">Contribution du vert au vert</translation>
    </message>
    <message>
        <source>Contribution of blue to green</source>
        <translation type="vanished">Contribution du bleu au vert</translation>
    </message>
    <message>
        <source>Contribution of red to blue</source>
        <translation type="vanished">Contribution du rouge au bleu</translation>
    </message>
    <message>
        <source>Contribution of green to blue</source>
        <translation type="vanished">Contribution du vert au bleu</translation>
    </message>
    <message>
        <source>Contribution of blue to blue</source>
        <translation type="vanished">Contribution du bleu au bleu</translation>
    </message>
    <message>
        <source>Vibrance</source>
        <translation type="obsolete">Vibrance</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="vanished">Rouge</translation>
    </message>
    <message>
        <source>Intensity red</source>
        <translation type="obsolete">Intensité du rouge</translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="vanished">Vert</translation>
    </message>
    <message>
        <source>Intensity green</source>
        <translation type="obsolete">Intensité du vert</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="vanished">Bleu</translation>
    </message>
    <message>
        <source>Intensity blue</source>
        <translation type="obsolete">Intensité du bleu</translation>
    </message>
    <message>
        <source>Enhance shadows</source>
        <translation type="obsolete">Améliorer les ombres</translation>
    </message>
    <message>
        <source>Enhance shadows only</source>
        <translation type="obsolete">Améliorer seulement les ombres</translation>
    </message>
    <message>
        <source>Enhance highlights</source>
        <translation type="obsolete">Améliorer les hautes lumières</translation>
    </message>
    <message>
        <source>Enhance highlights only</source>
        <translation type="obsolete">Améliorer seulement les hautes lumières</translation>
    </message>
    <message>
        <source>Highlights R</source>
        <translation type="obsolete">Hautes lumières R</translation>
    </message>
    <message>
        <source>Adjust the brightness of the highlights in R</source>
        <translation type="obsolete">Ajuster la luminosité des hautes lumière dans R</translation>
    </message>
    <message>
        <source>Highlights G</source>
        <translation type="obsolete">Hautes lumières V</translation>
    </message>
    <message>
        <source>Adjust the brightness of the highlights in G</source>
        <translation type="obsolete">Ajuster la luminosité des hautes lumière dans V</translation>
    </message>
    <message>
        <source>Highlights B</source>
        <translation type="obsolete">Hautes lumières B</translation>
    </message>
    <message>
        <source>Adjust the brightness of the highlights in B</source>
        <translation type="obsolete">Ajuster la luminosité des hautes lumière dans B</translation>
    </message>
    <message>
        <source>% white</source>
        <translation type="vanished">% blanc</translation>
    </message>
    <message>
        <source>Percentage of white aimed at</source>
        <translation type="vanished">Pourcentage de blanc visé</translation>
    </message>
    <message>
        <source>WhiteLevel</source>
        <translation type="vanished">Niveau de blanc</translation>
    </message>
    <message>
        <source>EV</source>
        <translation type="vanished">IL</translation>
    </message>
    <message>
        <source>Exposure in EV</source>
        <translation type="vanished">Exposition en IL</translation>
    </message>
    <message>
        <source>Exposure gain</source>
        <translation type="obsolete">Gain d&apos;exposition</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="obsolete">Luminosité</translation>
    </message>
    <message>
        <source>Chrominance adaption</source>
        <translation type="obsolete">Adaptation de la chrominance</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="obsolete">Lumière</translation>
    </message>
    <message>
        <source>Light adaption</source>
        <translation type="obsolete">Adaptation de lumière</translation>
    </message>
    <message>
        <source>Catch white</source>
        <translation type="obsolete">Capturer le blanc</translation>
    </message>
    <message>
        <source>Darken just the brightest parts</source>
        <translation type="obsolete">N&apos;assombrir que les parties les plus claires</translation>
    </message>
    <message>
        <source>Catch black</source>
        <translation type="obsolete">Capturer le noir</translation>
    </message>
    <message>
        <source>Brighten just the darkest parts</source>
        <translation type="obsolete">N&apos;éclaircir que les parties les plus sombres</translation>
    </message>
    <message>
        <source>Amount</source>
        <translation type="vanished">Niveau</translation>
    </message>
    <message>
        <source>Amount of recovery</source>
        <translation type="obsolete">Niveau de récupération</translation>
    </message>
    <message>
        <source>Lower Limit</source>
        <translation type="vanished">Limite inférieure</translation>
    </message>
    <message>
        <source>Upper Limit</source>
        <translation type="vanished">Limite supérieure</translation>
    </message>
    <message>
        <source>Softness</source>
        <translation type="vanished">Douceur</translation>
    </message>
    <message>
        <source>Threshold</source>
        <translation type="vanished">Seuil</translation>
    </message>
    <message>
        <source>Opacity</source>
        <translation type="vanished">Opacité</translation>
    </message>
    <message>
        <source>Denoise</source>
        <translation type="vanished">Débruitage</translation>
    </message>
    <message>
        <source>Don&apos;t amplify noise</source>
        <translation type="vanished">Ne pas amplifier le bruit</translation>
    </message>
    <message>
        <source>Masking</source>
        <translation type="vanished">Masquage</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="vanished">Rayon</translation>
    </message>
    <message>
        <source>Halo Control</source>
        <translation type="vanished">Contrôle du halo</translation>
    </message>
    <message>
        <source>Fine Detail</source>
        <translation type="obsolete">Détails fins</translation>
    </message>
    <message>
        <source>Coarse Detail</source>
        <translation type="obsolete">Détails grossiers</translation>
    </message>
    <message>
        <source>Amount of compression</source>
        <translation type="obsolete">Niveau de compression</translation>
    </message>
    <message>
        <source>Bias</source>
        <translation type="obsolete">Biais</translation>
    </message>
    <message>
        <source>Bias of compression</source>
        <translation type="obsolete">Biais de compression</translation>
    </message>
    <message>
        <source>Color Adaption</source>
        <translation type="obsolete">Adaptation de couleur</translation>
    </message>
    <message>
        <source>Color adaption</source>
        <translation type="obsolete">Adaptation de couleur</translation>
    </message>
    <message>
        <source>Feather</source>
        <translation type="vanished">Plume</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="130"/>
        <source>Gamma</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="131"/>
        <source>Linearity</source>
        <translation>Linéarité</translation>
    </message>
    <message>
        <source>Contrast</source>
        <translation type="vanished">Contraste</translation>
    </message>
    <message>
        <source>Amount of contrast</source>
        <translation type="obsolete">Niveau de contraste</translation>
    </message>
    <message>
        <source>Threshold for contrast</source>
        <translation type="obsolete">Seuil pour le contraste</translation>
    </message>
    <message>
        <source>Amount of saturation</source>
        <translation type="obsolete">Niveau de saturation</translation>
    </message>
    <message>
        <source>Value A</source>
        <translation type="obsolete">Valeur A</translation>
    </message>
    <message>
        <source>Amount of boosting A</source>
        <translation type="obsolete">Niveau de renforcement de A</translation>
    </message>
    <message>
        <source>Value B</source>
        <translation type="obsolete">Valeur B</translation>
    </message>
    <message>
        <source>Amount of boosting B</source>
        <translation type="obsolete">Niveau de renforcement de B</translation>
    </message>
    <message>
        <source>Master</source>
        <translation type="vanished">Maître</translation>
    </message>
    <message>
        <source>Quick setup for the levels</source>
        <translation type="vanished">Réglage rapide des niveaux</translation>
    </message>
    <message>
        <source>Threshold on L</source>
        <translation type="vanished">Seuil sur L</translation>
    </message>
    <message>
        <source>Threshold on color</source>
        <translation type="vanished">Seuil sur la couleur</translation>
    </message>
    <message>
        <source>Level 1</source>
        <translation type="vanished">Niveau 1</translation>
    </message>
    <message>
        <source>Boosting of level 1</source>
        <translation type="vanished">Renforcement du niveau 1</translation>
    </message>
    <message>
        <source>Level 2</source>
        <translation type="vanished">Niveau 2</translation>
    </message>
    <message>
        <source>Boosting of level 2</source>
        <translation type="vanished">Renforcement du niveau 2</translation>
    </message>
    <message>
        <source>Level 3</source>
        <translation type="vanished">Niveau 3</translation>
    </message>
    <message>
        <source>Boosting of level 3</source>
        <translation type="vanished">Renforcement du niveau 3</translation>
    </message>
    <message>
        <source>Level 4</source>
        <translation type="vanished">Niveau 4</translation>
    </message>
    <message>
        <source>Boosting of level 4</source>
        <translation type="vanished">Renforcement du niveau 4</translation>
    </message>
    <message>
        <source>Level 5</source>
        <translation type="vanished">Niveau 5</translation>
    </message>
    <message>
        <source>Boosting of level 5</source>
        <translation type="vanished">Renforcement du niveau 5</translation>
    </message>
    <message>
        <source>Level 6</source>
        <translation type="vanished">Niveau 6</translation>
    </message>
    <message>
        <source>Boosting of level 6</source>
        <translation type="vanished">Renforcement du niveau 6</translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation type="vanished">Amplitude</translation>
    </message>
    <message>
        <source>Iterations</source>
        <translation type="vanished">Itérations</translation>
    </message>
    <message>
        <source>Sharpness</source>
        <translation type="vanished">Dureté</translation>
    </message>
    <message>
        <source>Anisotropy</source>
        <translation type="vanished">Anisotropie</translation>
    </message>
    <message>
        <source>Gradient smoothness</source>
        <translation type="vanished">Douceur du gradient</translation>
    </message>
    <message>
        <source>Tensor smoothness</source>
        <translation type="vanished">Douceur du tenseur</translation>
    </message>
    <message>
        <source>Spacial precision</source>
        <translation type="vanished">Précision spatiale</translation>
    </message>
    <message>
        <source>Angular precision</source>
        <translation type="vanished">Précision angulaire</translation>
    </message>
    <message>
        <source>Value precision</source>
        <translation type="vanished">Précision de la valeur</translation>
    </message>
    <message>
        <source>Tune masks</source>
        <translation type="vanished">Réglage des masques</translation>
    </message>
    <message>
        <source>Fine tune the color masks</source>
        <translation type="vanished">Réglage fin des masques de couleurs</translation>
    </message>
    <message>
        <source>L amount</source>
        <translation type="vanished">Niveau L</translation>
    </message>
    <message>
        <source>Denoise amount on L</source>
        <translation type="vanished">Niveau de débruitage sur L</translation>
    </message>
    <message>
        <source>Color amount</source>
        <translation type="vanished">Quantité de couleur</translation>
    </message>
    <message>
        <source>Denoise amount on AB</source>
        <translation type="vanished">Niveau de débruitage sur AB</translation>
    </message>
    <message>
        <source>Levels</source>
        <translation type="vanished">Niveaux</translation>
    </message>
    <message>
        <source>Opacity of denoising on L</source>
        <translation type="vanished">Opacité pour le débruitage sur L</translation>
    </message>
    <message>
        <source>Edge Threshold</source>
        <translation type="vanished">Seuil des bords</translation>
    </message>
    <message>
        <source>Edge thresholding for denoising on L</source>
        <translation type="vanished">Seuillage des bords pour le débruitage sur L</translation>
    </message>
    <message>
        <source>L scale</source>
        <translation type="vanished">Échelle pour L</translation>
    </message>
    <message>
        <source>Denoise scale on L</source>
        <translation type="vanished">Échelle de débruitage sur L</translation>
    </message>
    <message>
        <source>Denoise on L</source>
        <translation type="vanished">Débruiter sur L</translation>
    </message>
    <message>
        <source>A amount</source>
        <translation type="vanished">Niveau de A</translation>
    </message>
    <message>
        <source>Color A denoise</source>
        <translation type="vanished">Débruitage de la couleur A</translation>
    </message>
    <message>
        <source>A scale</source>
        <translation type="vanished">Échelle pour A</translation>
    </message>
    <message>
        <source>Denoise scale on A</source>
        <translation type="vanished">Échelle de débruitage sur A</translation>
    </message>
    <message>
        <source>B amount</source>
        <translation type="vanished">Niveau de B</translation>
    </message>
    <message>
        <source>Color B denoise</source>
        <translation type="vanished">Débruitage de la couleur B</translation>
    </message>
    <message>
        <source>B scale</source>
        <translation type="vanished">Échelle pour B</translation>
    </message>
    <message>
        <source>Denoise scale on B</source>
        <translation type="vanished">Échelle de débruitage sur B</translation>
    </message>
    <message>
        <source>Threshold for wavelet L denoise (with edge mask)</source>
        <translation type="vanished">Seuil pour le débruitage en ondelettes sur L (avec masque de bords)</translation>
    </message>
    <message>
        <source>L softness</source>
        <translation type="vanished">Douceur pour L</translation>
    </message>
    <message>
        <source>Softness for wavelet L denoise (with edge mask)</source>
        <translation type="vanished">Douceur pour le débruitage en ondelettes sur L (avec masque de bords)</translation>
    </message>
    <message>
        <source>Threshold for wavelet A denoise</source>
        <translation type="vanished">Seuil pour le débruitage en ondelettes sur A</translation>
    </message>
    <message>
        <source>A softness</source>
        <translation type="vanished">Douceur sur A</translation>
    </message>
    <message>
        <source>Softness for wavelet A denoise</source>
        <translation type="vanished">Douceur pour le débruitage en ondelettes sur A</translation>
    </message>
    <message>
        <source>Threshold for wavelet B denoise</source>
        <translation type="vanished">Seuil pour le débruitage en ondelettes sur B</translation>
    </message>
    <message>
        <source>B softness</source>
        <translation type="vanished">Douceur sur B</translation>
    </message>
    <message>
        <source>Softness for wavelet B denoise</source>
        <translation type="vanished">Douceur pour le débruitage en ondelettes sur B</translation>
    </message>
    <message>
        <source>Number of passes</source>
        <translation type="vanished">Nombre de passes</translation>
    </message>
    <message>
        <source>Strength</source>
        <translation type="vanished">Force</translation>
    </message>
    <message>
        <source>Halo control</source>
        <translation type="vanished">Contrôle du halo</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="vanished">Poids</translation>
    </message>
    <message>
        <source>Clean up</source>
        <translation type="vanished">Nettoyage</translation>
    </message>
    <message>
        <source>Microcontrast</source>
        <translation type="vanished">Micro-contraste</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="61"/>
        <source>Thumbnail size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="61"/>
        <source>Thumbnail size in pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="62"/>
        <source>Thumbnail padding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="62"/>
        <source>Thumbnail padding in pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="63"/>
        <source>Thumbnails in a row/column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="63"/>
        <location filename="../Sources/ptSettings.cpp" line="191"/>
        <source>Maximum number of thumbnails that should be placed in a row or column.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="64"/>
        <source>Thumbnail export size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="64"/>
        <source>Thumbnail export size in pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Microcontrast strength</source>
        <translation type="vanished">Force du microcontraste</translation>
    </message>
    <message>
        <source>Microcontrast Halo control</source>
        <translation type="vanished">Contrôle du halo du microcontraste</translation>
    </message>
    <message>
        <source>Microcontrast weight</source>
        <translation type="vanished">Poids du microcontraste</translation>
    </message>
    <message>
        <source>Gaussian</source>
        <translation type="obsolete">Gaussien</translation>
    </message>
    <message>
        <source>Box</source>
        <translation type="obsolete">Cadre</translation>
    </message>
    <message>
        <source>Lens blur</source>
        <translation type="obsolete">Flou d&apos;objectif</translation>
    </message>
    <message>
        <source>Number of iterations</source>
        <translation type="vanished">Nombre d&apos;itérations</translation>
    </message>
    <message>
        <source>Radius for USM</source>
        <translation type="vanished">Rayon pour le masque flou</translation>
    </message>
    <message>
        <source>Amount for USM</source>
        <translation type="vanished">Niveau pour le masque flou</translation>
    </message>
    <message>
        <source>Threshold for USM</source>
        <translation type="vanished">Seuil pour le masque flou</translation>
    </message>
    <message>
        <source>Radius for Highpass</source>
        <translation type="vanished">Rayon pour le passe-haut</translation>
    </message>
    <message>
        <source>Amount for Highpass</source>
        <translation type="vanished">Niveau pour le passe-haut</translation>
    </message>
    <message>
        <source>Denoise for Highpass</source>
        <translation type="vanished">Débruitage pour le passe-haut</translation>
    </message>
    <message>
        <source>Strength for film grain</source>
        <translation type="vanished">Force du grain du film</translation>
    </message>
    <message>
        <source>Radius for film grain</source>
        <translation type="vanished">Rayon du grain du film</translation>
    </message>
    <message>
        <source>Opacity for film grain</source>
        <translation type="vanished">Opacité du grain du film</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="vanished">Saturation</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation type="obsolete">Teinte</translation>
    </message>
    <message>
        <source>Orange</source>
        <translation type="obsolete">Orange</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="vanished">Jaune</translation>
    </message>
    <message>
        <source>Light green</source>
        <translation type="obsolete">Vert clair</translation>
    </message>
    <message>
        <source>Dark green</source>
        <translation type="obsolete">Vert sombre</translation>
    </message>
    <message>
        <source>Cyan</source>
        <translation type="vanished">Cyan</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation type="vanished">Échelle</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="65"/>
        <source>Thumbnail cache (MB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="65"/>
        <source>Maximum size of thumbnail cache in MBytes.
Requires a restart to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="128"/>
        <source>Pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="129"/>
        <source>Image height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shape of the vignette</source>
        <translation type="vanished">Échelle de la vignette</translation>
    </message>
    <message>
        <source>Inner Radius</source>
        <translation type="vanished">Rayon interne</translation>
    </message>
    <message>
        <source>Outer Radius</source>
        <translation type="vanished">Rayon externe</translation>
    </message>
    <message>
        <source>Roundness</source>
        <translation type="vanished">Rondeur</translation>
    </message>
    <message>
        <source>Center X</source>
        <translation type="vanished">Centre X</translation>
    </message>
    <message>
        <source>Center Y</source>
        <translation type="vanished">Centre Y</translation>
    </message>
    <message>
        <source>Red multiplicity</source>
        <translation type="vanished">Facteur de rouge</translation>
    </message>
    <message>
        <source>Green multiplicity</source>
        <translation type="vanished">Facteur de vert</translation>
    </message>
    <message>
        <source>Blue multiplicity</source>
        <translation type="vanished">Facteur de bleu</translation>
    </message>
    <message>
        <source>Red toning</source>
        <translation type="vanished">Ton rouge</translation>
    </message>
    <message>
        <source>Green toning</source>
        <translation type="vanished">Ton vert</translation>
    </message>
    <message>
        <source>Blue toning</source>
        <translation type="vanished">Ton bleu</translation>
    </message>
    <message>
        <source>Amount of toning</source>
        <translation type="vanished">Niveau de réglage des tons</translation>
    </message>
    <message>
        <source>Main color</source>
        <translation type="vanished">Couleur principale</translation>
    </message>
    <message>
        <source>Intensity of the main color</source>
        <translation type="vanished">Intensité de la couleur principale</translation>
    </message>
    <message>
        <source>Second color</source>
        <translation type="vanished">Couleur secondaire</translation>
    </message>
    <message>
        <source>Intensity of the second color</source>
        <translation type="vanished">Intensité de la couleur secondaire</translation>
    </message>
    <message>
        <source>Shape of the mask</source>
        <translation type="vanished">Échelle du masque</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="vanished">Angle</translation>
    </message>
    <message>
        <source>Lower Level</source>
        <translation type="vanished">Niveau inférieur</translation>
    </message>
    <message>
        <source>Upper Level</source>
        <translation type="vanished">Niveau supérieur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="133"/>
        <source>Quality</source>
        <translation>Qualité</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="134"/>
        <source>dpi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="134"/>
        <source>Resolution in dpi</source>
        <translation>Résolution en dpi</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="135"/>
        <source>Rating</source>
        <translation>Notation</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="135"/>
        <source>Image rating</source>
        <translation>Notation de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="141"/>
        <source>File for autosaving batch list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="142"/>
        <source>Remember setting level</source>
        <translation>Se souvenir du niveau de la configuration</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="143"/>
        <source>Transform camera RGB to working space RGB</source>
        <translation>Transformer RVB de l&apos;appareil vers RVB de l&apos;espace de travail</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="144"/>
        <location filename="../Sources/ptSettings.cpp" line="148"/>
        <location filename="../Sources/ptSettings.cpp" line="149"/>
        <source>Intent of the profile</source>
        <translation>But du profil</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="145"/>
        <source>Gamma that was applied before this profile</source>
        <translation>Gamma qui a été appliqué avant ce profil</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="146"/>
        <source>Working colorspace</source>
        <translation>Espace de couleurs de travail</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="147"/>
        <source>Color management quality</source>
        <translation>Qualité de la gestion de couleur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="150"/>
        <source>Output mode of save button</source>
        <translation>Mode de sortie du bouton d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="151"/>
        <source>Output mode of reset button</source>
        <translation>Mode de sortie du bouton de RàZ</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="152"/>
        <source>Set the theme.</source>
        <translation>Définir le thème.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="153"/>
        <source>Set the highlight color of the theme.</source>
        <translation>Définir la couleur de surlignage du thème.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="154"/>
        <source>Set the start up mode for the UI.</source>
        <translation>Définir le mode de démarrage de l&apos;IHM.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="155"/>
        <source>Size of image processed vs original.</source>
        <translation>Taille de l&apos;image traitée par rapport à l&apos;originale.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="156"/>
        <source>Initial pipe size when Photivo starts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="157"/>
        <source>Special preview for image analysis</source>
        <translation>Aperçu spécial pour l&apos;analyse de l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="158"/>
        <source>Bad pixels file</source>
        <translation>Fichier de pixels morts</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="159"/>
        <source>Darkframe file</source>
        <translation>Image noire (dark)</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="160"/>
        <source>WhiteBalance</source>
        <translation>Balance des blancs</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="161"/>
        <source>CA correction</source>
        <translation>Correction des AC</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="162"/>
        <source>Demosaicing algorithm</source>
        <translation>Algorithme de dématriçage</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="163"/>
        <source>Denosie on Bayer pattern</source>
        <translation>Débruitage dans la structure de Bayer</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="165"/>
        <source>Guide lines for crop</source>
        <translation>Lignes guides pour la découpe</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="166"/>
        <source>Dim areas outside the crop rectangle</source>
        <translation>Obscurcir les zones à l&apos;extérieur du rectangle de découpe</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="167"/>
        <source>How to handle clipping</source>
        <translation>Comment gérer la sous/sur-exposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="168"/>
        <source>Mathematical model for CA correction</source>
        <translation>Modèle mathématique pour les AC</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="169"/>
        <source>Mathematical model for vignetting correction</source>
        <translation>Modèle mathématique pour la correction du vignettage</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="170"/>
        <source>Geometry of the lens the image was taken with</source>
        <translation>Géométrie de l&apos;objectif qui a servi à la prise de vue</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="171"/>
        <source>Convert image to this lens geometry</source>
        <translation>Convertir l&apos;image vers cette géométrie d&apos;objectif</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="172"/>
        <source>Mathematical distortion model to apply to the image</source>
        <translation>Modèle mathématique de distorsion à appliquer à l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="173"/>
        <source>Energy method for liquid rescale</source>
        <translation>Méthode de l&apos;énergie pour Liquid rescale</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="174"/>
        <source>Scaling method for liquid rescale</source>
        <translation>Méthode pour Liquid rescale</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="175"/>
        <location filename="../Sources/ptSettings.cpp" line="182"/>
        <source>Filter to be used for resizing</source>
        <translation>Filtre à utiliser pour le redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="176"/>
        <location filename="../Sources/ptSettings.cpp" line="181"/>
        <source>Image dimension the resize value applies to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="177"/>
        <source>Flip mode</source>
        <translation>Mode de retournement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="178"/>
        <source>Aspect width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="179"/>
        <source>Aspect height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>ChannelMixer</source>
        <translation type="vanished">Mixeur de canaux</translation>
    </message>
    <message>
        <source>Clip mode</source>
        <translation type="vanished">Mode de sous/sur-exposition</translation>
    </message>
    <message>
        <source>Auto exposure mode</source>
        <translation type="vanished">Mode d&apos;exposition auto</translation>
    </message>
    <message>
        <source>LAB Transform mode</source>
        <translation type="obsolete">Mode de transformation Lab</translation>
    </message>
    <message>
        <source>Values for recovery</source>
        <translation type="obsolete">Valeurs pour la récupération</translation>
    </message>
    <message>
        <source>Values for microcontrast</source>
        <translation type="vanished">Valeur pour le micro-contraste</translation>
    </message>
    <message>
        <source>Enable GreyCStoration on L</source>
        <translation type="vanished">Activer GreyCStoration sur L</translation>
    </message>
    <message>
        <source>GREYC Interpolation</source>
        <translation type="vanished">Interpolation GREYC</translation>
    </message>
    <message>
        <source>Enable USM sharpening</source>
        <translation type="vanished">Activer l&apos;accentuation par masque flou</translation>
    </message>
    <message>
        <source>Enable Highpass sharpening</source>
        <translation type="vanished">Activer l&apos;accentuation passe-haut</translation>
    </message>
    <message>
        <source>Values for film grain</source>
        <translation type="vanished">Valeurs du grain du film</translation>
    </message>
    <message>
        <source>Mode for film grain</source>
        <translation type="vanished">Mode pour le grain du film</translation>
    </message>
    <message>
        <source>Mode for Vignette</source>
        <translation type="vanished">Mode pour la vignette</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="185"/>
        <source>Switch to this zoom level when starting to crop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="191"/>
        <source>At most</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="192"/>
        <source>Open file manager on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="192"/>
        <source>Opens the file manager when Photivo starts without an image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="194"/>
        <source>Automatically save batch list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="194"/>
        <source>Automatically save current batch list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="195"/>
        <source>Automatically load batch list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="195"/>
        <source>Automatically load previous batch list saved to standard path on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="227"/>
        <source>Autosave settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="227"/>
        <source>Autosave settings when loading another image (if save confirmation is off)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="230"/>
        <source>Esc key exits Photivo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="230"/>
        <source>Use the Esc key not only to exit special view modes (e.g. full screen) but also to close Photivo.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RGB curve</source>
        <translation type="obsolete">Courbe RVB</translation>
    </message>
    <message>
        <source>R curve</source>
        <translation type="obsolete">Courbe R</translation>
    </message>
    <message>
        <source>G curve</source>
        <translation type="obsolete">Courbe V</translation>
    </message>
    <message>
        <source>B curve</source>
        <translation type="obsolete">Courbe B</translation>
    </message>
    <message>
        <source>L curve</source>
        <translation type="obsolete">Courbe L</translation>
    </message>
    <message>
        <source>a curve</source>
        <translation type="obsolete">Courbe a</translation>
    </message>
    <message>
        <source>b curve</source>
        <translation type="obsolete">Courbe b</translation>
    </message>
    <message>
        <source>L by hue curve</source>
        <translation type="obsolete">L par courbe de teinte</translation>
    </message>
    <message>
        <source>Hue curve</source>
        <translation type="obsolete">Courbe de teinte</translation>
    </message>
    <message>
        <source>Texture curve</source>
        <translation type="obsolete">Courbe de texture</translation>
    </message>
    <message>
        <source>Saturation curve</source>
        <translation type="obsolete">Courbe de saturation</translation>
    </message>
    <message>
        <source>Base curve</source>
        <translation type="obsolete">Courbe de base</translation>
    </message>
    <message>
        <source>After gamma curve</source>
        <translation type="obsolete">Courbe post-gamma</translation>
    </message>
    <message>
        <source>Shadows / Highlights curve</source>
        <translation type="obsolete">Courbe des Ombres / Hautes lumières</translation>
    </message>
    <message>
        <source>Detail curve</source>
        <translation type="obsolete">Courbe de détail</translation>
    </message>
    <message>
        <source>Denoise curve</source>
        <translation type="obsolete">Courbe de débruitage</translation>
    </message>
    <message>
        <source>View seperate LAB channels</source>
        <translation type="vanished">Voir les canaux Lab séparés</translation>
    </message>
    <message>
        <source>Values for tone adjustment</source>
        <translation type="obsolete">Valeurs pour l&apos;ajustement de ton</translation>
    </message>
    <message>
        <source>Film emulation</source>
        <translation type="vanished">Émulation de film</translation>
    </message>
    <message>
        <source>Color filter emulation</source>
        <translation type="vanished">Émulatiion de filtre de couleur</translation>
    </message>
    <message>
        <source>Values for Toning</source>
        <translation type="vanished">Valeurs pour l&apos;ajustement de tons</translation>
    </message>
    <message>
        <source>Colors for cross processing</source>
        <translation type="vanished">Couleurs pour le traitement croisé</translation>
    </message>
    <message>
        <source>Mode for Texture Overlay</source>
        <translation type="vanished">Mode pour le calque de texture</translation>
    </message>
    <message>
        <source>Mask for Texture Overlay</source>
        <translation type="vanished">Masque pour le calque de texture</translation>
    </message>
    <message>
        <source>Mode for Gradual Overlay</source>
        <translation type="vanished">Mode pour le calque dégradé</translation>
    </message>
    <message>
        <source>Mode for Softglow</source>
        <translation type="vanished">Mode pour le filtre adoucissant</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="180"/>
        <source>Enable web resizing</source>
        <translation>Activer le redimensionnement pour le web</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="183"/>
        <source>Output format</source>
        <translation>Format de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="184"/>
        <source>JPEG color sampling</source>
        <translation>Quantification des couleurs JPEG</translation>
    </message>
    <message>
        <source>Output mode</source>
        <translation type="obsolete">Mode de sortie</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="197"/>
        <source>User settings</source>
        <translation>Configuration utilisateur</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="197"/>
        <source>Load user settings on startup</source>
        <translation>Charger la configuration utilisateur au démarrage</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="198"/>
        <source>Reset on new image</source>
        <translation>Remettre à zéro pour une nouvelle image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="198"/>
        <source>Reset to user settings when new image is opened</source>
        <translation>Recharger la configuration utilisateur quand une nouvelle images est ouverte</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="199"/>
        <source>Adjust aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="199"/>
        <source>Adjust crop aspect ratio to image aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="200"/>
        <source>Nonlinear slider response</source>
        <translation>Réponse non linéaire des glissières</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="200"/>
        <source>Alter the slider behaviour</source>
        <translation>Modifier le comportement des glissières</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="201"/>
        <source>Use gimp plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="201"/>
        <source>Use gimp plugin for export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="202"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="202"/>
        <source>Show seperate toolboxes</source>
        <translation>Afficher des boîtes à outils séparées</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="203"/>
        <source>Tab mode</source>
        <translation>Onglets</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="203"/>
        <source>Show the preview after the active tab</source>
        <translation>Afficher l&apos;aperçu après l&apos;onglet actif</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="204"/>
        <source>Override default</source>
        <translation>Remplacer défaut</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="204"/>
        <source>Override the default color</source>
        <translation>Remplacer la couleur par défaut</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="205"/>
        <source>Display search bar</source>
        <translation>Afficher la barre de recherche</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="206"/>
        <source>Backup settings</source>
        <translation>Configuration de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="206"/>
        <source>Write backup settings during processing</source>
        <translation>Écrire la configuration de sauvegarde pendant le traitement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="207"/>
        <source>manual</source>
        <translation>manuel</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="207"/>
        <source>manual or automatic pipe</source>
        <translation>Tube manuel ou automatique</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="208"/>
        <source>Use thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="208"/>
        <source>Use the embedded thumbnail of RAW images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Intensify</source>
        <translation type="vanished">Intensifier</translation>
    </message>
    <message>
        <source>Normalize lowest channel to 1</source>
        <translation type="vanished">Normaliser le canal le plus faible à 1</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="209"/>
        <source>Manual BP</source>
        <translation>PN manuel</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="209"/>
        <source>Manual black point setting enabled</source>
        <translation>Définition du point noir activée</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="210"/>
        <source>Manual WP</source>
        <translation>PB manuel</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="210"/>
        <source>Manual white point setting enabled</source>
        <translation>Définition du point blanc activée</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="211"/>
        <source>Eeci refinement</source>
        <translation>Amélioration eeci</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="212"/>
        <location filename="../Sources/ptSettings.cpp" line="214"/>
        <source>Auto scale</source>
        <translation>Redimensionnement automatique</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="212"/>
        <source>Auto scale to avoid black borders after distortion correction or geometry conversion.</source>
        <translation>Redimensionnement automatique pour éviter les bords noirs après correction de la distorsion ou conversion de la géométrie.</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="213"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="213"/>
        <source>Enable defishing</source>
        <translation>Activer la correction defish</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="214"/>
        <source>Auto scale to avoid black borders</source>
        <translation>Redimensionnement automatique pour éviter les bords noirs</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="215"/>
        <source>Grid</source>
        <translation>Grille</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="215"/>
        <source>Enable the overlay grid</source>
        <translation>Superposer la grille</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="216"/>
        <source>Crop</source>
        <translation>Découpage</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="216"/>
        <source>Enable to make a crop</source>
        <translation>Activer pour découper</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="217"/>
        <source>Aspect Ratio</source>
        <translation>Rapport d&apos;aspect</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="217"/>
        <source>Crop with a fixed aspect ratio</source>
        <translation>Découper avec un rapport fixe</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="218"/>
        <source>Vertical first</source>
        <translation>Commencer en vertical</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="218"/>
        <source>Resizing starts with vertical direction</source>
        <translation>Le redimensionnement commence par le sens vertical</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="219"/>
        <source>Resize</source>
        <translation>Redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="219"/>
        <source>Enable resize</source>
        <translation>Activer le redimensionnement</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="220"/>
        <source>Automatic pipe size</source>
        <translation>Taille automatique du tube</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="221"/>
        <source>Block pipe</source>
        <translation>Bloquer le tube</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="221"/>
        <source>Disable the pipe</source>
        <translation>Désactiver le tube</translation>
    </message>
    <message>
        <source>Enable Reinhard 05</source>
        <translation type="obsolete">Activer Reinhard 05</translation>
    </message>
    <message>
        <source>Enable &apos;fast&apos;</source>
        <translation type="vanished">Activer &quot;rapide&quot;</translation>
    </message>
    <message>
        <source>Enable GREYC &apos;fast&apos;</source>
        <translation type="vanished">Activer GREYC &quot;rapide&quot;</translation>
    </message>
    <message>
        <source>Purple</source>
        <translation type="vanished">Violet</translation>
    </message>
    <message>
        <source>Enable wiener filter</source>
        <translation type="obsolete">Activer le filtre de Wiener</translation>
    </message>
    <message>
        <source>Only edges</source>
        <translation type="vanished">Bords seuls</translation>
    </message>
    <message>
        <source>Sharpen only edges</source>
        <translation type="vanished">Accentuer seulement les bords</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="222"/>
        <source>before gamma</source>
        <translation>avant gamma</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="222"/>
        <source>Webresizing before gamma compensation</source>
        <translation>Redimensionnement web avant compensation du gamma</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="223"/>
        <source>sRGB gamma compensation</source>
        <translation>Compensation du gamma sRVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="224"/>
        <source>Include metadata</source>
        <translation>Inclure les métadonnées</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="224"/>
        <source>Include metadata (only in jpeg and tiff)</source>
        <translation>Inclure les métadonnées (jpeg et tiff seulement)</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="225"/>
        <source>Erase thumbnail</source>
        <translation>Effacer l&apos;aperçu rapide</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="225"/>
        <source>Erase the exif thumbnail (only in jpeg and tiff)</source>
        <translation>Effacer l&apos;aperçu rapide exif (jpeg et tiff seulement)</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="226"/>
        <source>Save image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="226"/>
        <source>Confirm any action that would discard an unsaved image</source>
        <translation>Confirmer toute action susceptible d&apos;abandonner une image non sauvegardée</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="228"/>
        <source>Reset settings</source>
        <translation>Remettre la configuration à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="228"/>
        <source>Confirm resetting settings or dropping a settings file onto an image</source>
        <translation>Confirmer pour réinitialiser la configuration ou pour apppliquer un fichier de configuration déposé sur une image</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="229"/>
        <source>Switch to 1:1 pipe</source>
        <translation>Basculer vers le tube 1:1</translation>
    </message>
    <message>
        <location filename="../Sources/ptSettings.cpp" line="229"/>
        <source>Confirm switch to the full sized pipe</source>
        <translation>Confirmer le passage au tube pleine taille</translation>
    </message>
</context>
<context>
    <name>ptSingleDirModel</name>
    <message>
        <location filename="../Sources/filemgmt/ptSingleDirModel.cpp" line="158"/>
        <source>My Computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptSpotListWidget</name>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.ui" line="70"/>
        <source>Delete spot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.ui" line="87"/>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="145"/>
        <source>Append spot mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.ui" line="108"/>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="174"/>
        <source>Edit spots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="158"/>
        <source>Exit append spot mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="188"/>
        <source>Leave edit mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/filters/imagespot/ptSpotListWidget.cpp" line="237"/>
        <source>Spot</source>
        <translation type="unfinished">Pipette</translation>
    </message>
</context>
<context>
    <name>ptToolBox</name>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="148"/>
        <source>Open help page in web browser.</source>
        <translation>Ouvrir la page d&apos;aide dans le navigateur web.</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="154"/>
        <source>Complex filter. Might be slow.</source>
        <translation>Filtre complexe. Peut être lent.</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="241"/>
        <source>&amp;Reset</source>
        <translation>Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="245"/>
        <source>&amp;Save preset</source>
        <translation>Enregistrer un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="249"/>
        <source>&amp;Append preset</source>
        <translation>Ajouter un préréglage</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="257"/>
        <source>&amp;Hide</source>
        <translation>Masquer</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="275"/>
        <source>All&amp;ow</source>
        <translation type="unfinished">Autoriser</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="278"/>
        <source>Bl&amp;ock</source>
        <translation type="unfinished">Bloquer</translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="295"/>
        <source>Remove from &amp;favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptToolBox.cpp" line="298"/>
        <source>Add to &amp;favourites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ptViewWindow</name>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="676"/>
        <source>Zoom &amp;fit</source>
        <translation>Zoom adapté</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="668"/>
        <source>Zoom &amp;100%</source>
        <translation></translation>
    </message>
    <message>
        <source>Indicate</source>
        <translation type="obsolete">Indiquer</translation>
    </message>
    <message>
        <source>Indicate clipping</source>
        <translation type="obsolete">Indiquer la sous/sur-exposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="187"/>
        <source>Fit</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="652"/>
        <source>Copy settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="652"/>
        <source>Ctrl+Shift+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="655"/>
        <source>Paste settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="655"/>
        <source>Ctrl+Shift+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="658"/>
        <source>Reset settings</source>
        <translation type="unfinished">Remettre la configuration à zéro</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="658"/>
        <source>Ctrl+Shift+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="661"/>
        <source>Reset settings to last saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="661"/>
        <source>Ctrl+Shift+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="664"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="664"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="668"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="672"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="672"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="676"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="681"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="685"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="689"/>
        <source>&amp;L*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="689"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="693"/>
        <source>&amp;a*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="693"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="697"/>
        <source>&amp;b*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="697"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="701"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="713"/>
        <source>&amp;disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="717"/>
        <source>&amp;linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="721"/>
        <source>&amp;preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="736"/>
        <source>Highlight &amp;clipped pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="736"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="741"/>
        <source>&amp;Over exposure</source>
        <translation>Surexposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="746"/>
        <source>&amp;Under exposure</source>
        <translation>Sous-exposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="751"/>
        <source>&amp;R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="761"/>
        <source>&amp;B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="766"/>
        <source>&amp;Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="773"/>
        <source>Show &amp;bottom bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="788"/>
        <source>Open &amp;batch processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="788"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="791"/>
        <source>F11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="828"/>
        <source>Pixel values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="756"/>
        <source>&amp;G</source>
        <translation>&amp;V</translation>
    </message>
    <message>
        <source>Sensor</source>
        <translation type="obsolete">Capteur</translation>
    </message>
    <message>
        <source>Show zoom bar</source>
        <translation type="obsolete">Afficher la barre de zoom</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="778"/>
        <source>Show &amp;tool pane</source>
        <translation>Afficher les outils</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="778"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="784"/>
        <source>Open file m&amp;anager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="784"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="791"/>
        <source>Full&amp;screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="681"/>
        <source>&amp;RGB</source>
        <translation>RVB</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="701"/>
        <source>&amp;Gradient</source>
        <translation>Gradient</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="685"/>
        <source>&amp;Structure</source>
        <translation>Structure</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Done &lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt; Terminé &lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Updating &lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt; Mise à jour en cours &lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt; Processing &lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt; Traitement en cours &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="816"/>
        <source>Show &amp;clipping</source>
        <translation>Sous/Sur-exposition</translation>
    </message>
    <message>
        <location filename="../Sources/ptViewWindow.cpp" line="806"/>
        <source>Display &amp;mode</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ptVisibleToolsItemDelegate</name>
    <message>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="67"/>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="77"/>
        <source>Hidden</source>
        <translation>Masqué</translation>
    </message>
    <message>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="69"/>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="79"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="70"/>
        <location filename="../Sources/ptVisibleToolsView.cpp" line="81"/>
        <source>Favourite</source>
        <translation>Favori</translation>
    </message>
</context>
</TS>
